//
//  AssesmentRaceStartModel.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 09/09/22.
//

import Foundation

struct RaceStart: Identifiable {
    let id: Int
    let date: Date
}

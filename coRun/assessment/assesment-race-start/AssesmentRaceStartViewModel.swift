//
//  AssesmentRaceStartViewModel.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 09/09/22.
//

import Foundation

class AssesmentRaceStartViewModel: ObservableObject {
    @Published var selectedRace: String = ""
    
    
    func getListDate() -> [RaceStart] {
        var listDate = [RaceStart]()
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "e"
        let dayNow = dateFormatter.string(from: date)
        for i in 0...3 {
            let dateNow = Calendar.current.date(byAdding: .day, value: (i*7) + (7 - (Int(dayNow) ?? 0) + 2), to: Date())
            listDate.append(RaceStart(id: i, date: dateNow ?? Date()))
        }
        
        return listDate
    }
    
}


//
//  AssesmentReceStartItemView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 09/09/22.
//

import SwiftUI

struct AssesmentReceStartItemView: View {
    let raceStart: Date
    let selected: Bool
    var body: some View {
        Text(raceStart.formatted(with: "EEEE, dd MMMM yyyy"))
            .font(.system(size: 18, weight: selected ? .bold: .regular))
            .padding(.vertical, 30)
            .frame(maxWidth: .infinity)
            .background(Color("colorGray"))
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color("ColorButtonActive"), lineWidth: selected ? 1 : 0)
            )
    }
}

struct AssesmentReceStartItemView_Previews: PreviewProvider {
    static var previews: some View {
        AssesmentReceStartItemView(raceStart: Date(), selected: true)
    }
}

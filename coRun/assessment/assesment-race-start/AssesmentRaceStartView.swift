//
//  AssesmentRaceStartView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 09/09/22.
//

import SwiftUI

struct AssesmentRaceStartView: View {
    @StateObject var viewModel = AssesmentRaceStartViewModel()
    @State private var isShowingAssesmentLatestRunView = false
    @State var assesment: Assesment

    init(assesment: Assesment) {
        self.assesment = assesment
    }
    
    var body: some View {
        VStack{
            Text("When do you want to start the plan?")
                .font(.largeTitle)
                .multilineTextAlignment(.center)
                .padding()
            ScrollView(showsIndicators: false) {
                ForEach(viewModel.getListDate()) { raceStart in
                    AssesmentReceStartItemView(raceStart: raceStart.date, selected: viewModel.selectedRace == raceStart.date.formatted(with: "yyyy-MM-dd"))
                        .padding(EdgeInsets(top: 10, leading: 20, bottom: 0, trailing: 20))
                        .onTapGesture {
                            viewModel.selectedRace = raceStart.date.formatted(with: "yyyy-MM-dd")
                        }
                }
            }
            Text("4 of 5")
            Button(action: {
                assesment.raceStart = viewModel.selectedRace
                isShowingAssesmentLatestRunView = true
            }) {
                Text("Choose Goal")
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .font(.system(size: 18))
                    .padding()
                    .foregroundColor(.white)
                    .background(
                        RoundedRectangle(cornerRadius: 10, style: .continuous).fill(viewModel.selectedRace != "" ? Color("ColorButtonActive") : Color("ColorButtonDisable"))
                    )
            }
            .padding(.horizontal, 20)
            .disabled(viewModel.selectedRace == "")
            
            NavigationLink("Navigate To Latest Running",destination: AssesmentLatestRunningView(assesment: self.assesment), isActive: $isShowingAssesmentLatestRunView)
                .hidden()
//                .isDetailLink(false)
            
        }
        .navigationBarTitle(isShowingAssesmentLatestRunView ?  "Back" : "Assessment", displayMode: .inline)
    }
}

struct AssesmentRaceStartView_Previews: PreviewProvider {
    static var previews: some View {
        AssesmentRaceStartView(assesment: Assesment())
    }
}

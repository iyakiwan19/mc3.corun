//
//  AssesmentCreateView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 31/08/22.
//

import SwiftUI

struct AssesmentCreateView: View {
    @StateObject var viewModel = AssesmentCreateViewModel()
    @State private var isShowingAssesmentRunningLevelView = false
    
    @State var assesment: Assesment = Assesment()

    var body: some View {
        NavigationView {
            VStack{
                Text("What is your race goal?")
                    .font(.largeTitle)
                    .padding()
                ScrollView(showsIndicators: false) {
                    ForEach(dataRaceGoal) { raceGoal in
                        AssesmentCreateItemView(raceGoal: raceGoal.name, selected: viewModel.selectedRaceGoal == raceGoal.id)
                            .padding(EdgeInsets(top: 10, leading: 20, bottom: 0, trailing: 20))
                            .onTapGesture {
                                viewModel.selectedRaceGoal = raceGoal.id
                                assesment.raceGoal = String(raceGoal.id)
                            }
                    }
                }
                Text("1 of 5")
                Button(action: {
                    isShowingAssesmentRunningLevelView = true
                }) {
                    Text("Choose Goal")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .font(.system(size: 18))
                        .padding()
                        .foregroundColor(.white)
                        .background(
                            RoundedRectangle(cornerRadius: 10, style: .continuous).fill(viewModel.selectedRaceGoal >= 0 ? Color("ColorButtonActive") : Color("ColorButtonDisable"))
                        )
                }
                .padding(.horizontal, 20)
                .disabled(viewModel.selectedRaceGoal < 0)
                NavigationLink("Navigate To Running Level",destination: AssesmentRunningLevelView(assesment: self.assesment), isActive: $isShowingAssesmentRunningLevelView)
                    .hidden()
//                    .isDetailLink(false)
                    
            }
            .navigationBarTitle(isShowingAssesmentRunningLevelView ?  "Back" : "Assessment" , displayMode: .inline)
        }
        
    }
}

struct AssesmentCreateView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            AssesmentCreateView()
                .previewDevice("iPhone 13")
                .previewInterfaceOrientation(.portrait)
        }
    }
}

//
//  AssesmentCreateModel.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 31/08/22.
//

import Foundation

struct RaceGoal: Identifiable {
    let id: Int
    let name: String
}

var dataRaceGoal: [RaceGoal] = [
    .init(id: 0, name: "5k Race"),
    .init(id: 1, name: "10k Race2"),
    .init(id: 2, name: "21K Half Marathon Race"),
    .init(id: 3, name: "42K Marathon Race")
]

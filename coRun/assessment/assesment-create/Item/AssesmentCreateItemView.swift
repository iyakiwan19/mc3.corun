//
//  AssesmentCreateItemView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 31/08/22.
//

import SwiftUI

struct AssesmentCreateItemView: View {
    let raceGoal: String
    let selected: Bool
    var body: some View {
        Text(raceGoal)
            .font(.system(size: 18, weight: selected ? .bold: .regular))
            .padding(.vertical, 30)
            .frame(maxWidth: .infinity)
            .background(Color("colorGray"))
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color("ColorButtonActive"), lineWidth: selected ? 1 : 0)
            )
    }
}

struct AssesmentCreateItemView_Previews: PreviewProvider {
    static var previews: some View {
        AssesmentCreateItemView(raceGoal: "5k Race", selected: true)
    }
}

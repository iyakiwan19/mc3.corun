//
//  AssesmentCreateViewModel.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 31/08/22.
//

import Foundation

class AssesmentCreateViewModel: ObservableObject {
    @Published var selectedRaceGoal: Int = -1
}

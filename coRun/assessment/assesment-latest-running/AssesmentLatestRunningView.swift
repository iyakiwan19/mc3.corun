//
//  AssesmentLatestRunningView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 01/09/22.
//

import SwiftUI
import HealthKit

struct AssesmentLatestRunningView: View {
    @StateObject var viewModel = AssesmentLatestRunningViewModel()
    @State var assesment: Assesment = Assesment()

    @FocusState var keyboardFocus: Bool
    
    @AppStorage("coachID") var coachID = ""
    @AppStorage("coruntoken") var token = ""
    
    let location = ["Indoor", "Outdoor"]
    var disableForm: Bool {
        viewModel.dateFormated.isEmpty ||
        viewModel.distance.isEmpty || viewModel.pace.isEmpty || viewModel.duration.isEmpty
    }
    
    var body: some View {
        VStack{
            Text("Your latest running")
                .font(.largeTitle)
                .padding()
            ScrollView(showsIndicators: false) {
                VStack(alignment: .leading) {
                    Text("Date")
                        .font(.body)
                    if(viewModel.showPickerDateView){
                        HStack{
                            Spacer()
                            Button(action: {
                                withAnimation(.easeIn){
                                    viewModel.dateFormated = viewModel.date.formatted(with: "dd MMM yyyy")
                                    viewModel.showPickerDateView = false                          }
                            }) {
                                Text("Done")
                            }
                        }
                        HStack{
                            Spacer()
                            DatePicker("", selection: $viewModel.date, in: ...Date(), displayedComponents: .date)
                                .datePickerStyle(.wheel)
                                .labelsHidden()
                            Spacer()
                        }
                    } else {
                        TextField("", text: $viewModel.dateFormated)
                            .padding()
                            .font(.body)
                            .frame(height: 44)
                            .background(Color("ColorTextFieldGrey"))
                            .keyboardType(.numberPad)
                            .cornerRadius(10)
                            .disabled(true)
                            .onTapGesture {
                                withAnimation(.easeIn){
                                    viewModel.showPickerDateView = true
                                }
                            }
                    }
                    
                }
                
                VStack(alignment: .leading) {
                    Text("Distance (km)")
                        .font(.body)
                    TextField("", text: $viewModel.distance)
                        .padding()
                        .font(.body)
                        .frame(height: 44)
                        .background(Color("ColorTextFieldGrey"))
                        .keyboardType(.decimalPad)
                        .cornerRadius(10)
                        .focused($keyboardFocus)
                        .disableAutocorrection(true)
                }
                
                VStack(alignment: .leading) {
                    Text("Pace (min/km)")
                        .font(.body)
                    if(viewModel.showPickerPaceView) {
                        VStack(alignment: .trailing) {
                            Button(action: {
                                withAnimation(.easeIn){
                                    showResultPacePicker()
                                }
                            }) {
                                Text("Done")
                            }
                            AssesmentLatestRunningPacePickerView(viewModel: viewModel)
                                .frame(minWidth: 350, minHeight: 180)
                        }
                    } else {
                        TextField("", text: $viewModel.pace)
                            .padding()
                            .font(.body)
                            .frame(height: 44)
                            .background(Color("ColorTextFieldGrey"))
                            .keyboardType(.numberPad)
                            .cornerRadius(10)
                            .disabled(true)
                            .onTapGesture {
                                withAnimation(.easeIn){
                                    viewModel.showPickerPaceView = true
                                    showResultDurationPicker()
                                }
                            }
                    }
                    
                }
                VStack(alignment: .leading) {
                    Text("Duration (hh:mm:ss)")
                        .font(.body)
                    if(viewModel.showPickerDurationView) {
                        VStack(alignment: .trailing) {
                            Button(action: {
                                withAnimation(.easeIn){
                                    showResultDurationPicker()
                                }
                            }) {
                                Text("Done")
                            }
                            AssesmentLatestRunningDurationView(viewModel: viewModel)
                                .frame(minWidth: 350, minHeight: 180)
                        }
                    } else {
                        TextField("", text: $viewModel.duration)
                            .padding()
                            .font(.body)
                            .frame(height: 44)
                            .background(Color("ColorTextFieldGrey"))
                            .keyboardType(.numberPad)
                            .cornerRadius(10)
                            .focused($keyboardFocus)
                            .disabled(true)
                            .onTapGesture {
                                withAnimation(.easeIn){
                                    viewModel.showPickerDurationView = true
                                    showResultPacePicker()
                                }
                            }
                    }
                }
                VStack(alignment: .leading) {
                    Text("Location")
                        .font(.body)
                    Picker("segmented", selection: $viewModel.selectedLocation) {
                        ForEach(location, id: \.self) {
                            Text($0)
                        }
                    }
                    .pickerStyle(.segmented)
                    
                }
            }
            
            HStack(spacing: 0){
                Text("or ")
                Button(action: {
                    Task {
                        await requestPermissionHealtKit()
                        let output = await HealthKitUtils().readWorkouts()
                        let outputWorkouts = HealthKitUtils().convertRunningData(output ?? [])
                        
                        if outputWorkouts.count > 0 {
                            let fixWorkOut = outputWorkouts[0]
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            let dateLatest = dateFormatter.date(from: fixWorkOut.date ?? "")
                            
                            viewModel.date = dateLatest ?? Date()
                            viewModel.dateFormated = dateLatest?.formatted(with: "dd MMM yyyy") ?? ""
                            
                            viewModel.distance = String(fixWorkOut.distance ?? 0.0)
                            
                            var listPace = fixWorkOut.pace?.split(separator: ":")
                            listPace?.reverse()
                            
                            viewModel.secondPaceSelection = Int( listPace?[0] ?? "") ?? 0
                            viewModel.minutePaceSelection = Int( listPace?[1] ?? "") ?? 0
                            showResultPacePicker()
                            
                            var listDuration = fixWorkOut.duration?.split(separator: ":")
                            listDuration?.reverse()
                            
                            viewModel.secondDurationSelection = Int( listDuration?[0] ?? "") ?? 0
                            viewModel.minuteDurationSelection = Int( listDuration?[1] ?? "") ?? 0
                            if listDuration?.count ?? 0 > 2 {
                                viewModel.hourDurationSelection = Int( listDuration?[2] ?? "") ?? 0
                            } else {
                                viewModel.hourDurationSelection = 0
                            }
                            showResultDurationPicker()
                            
                            viewModel.selectedLocation = (fixWorkOut.indoor ?? true) ? "Indoor" : "Outdoor"
                        }
                        print(outputWorkouts)
                    }
                }) {
                    Text("Import from Apple Health").foregroundColor(Color("ColorButtonActive"))
                }
            }
            .padding(20)
            
            Text("5 of 5")
            
            Button(action: {
                viewModel.postCreateAsessement(assesment: self.assesment, token: token, caochID: coachID)
            }) {
                Text("Submit To Coach")
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .font(.system(size: 18))
                    .padding()
                    .foregroundColor(.white)
                    .background(
                        RoundedRectangle(cornerRadius: 10, style: .continuous).fill( disableForm ?  Color("ColorButtonDisable") : Color("ColorButtonActive"))
                    )
            }
            .disabled(disableForm)
            .alert(isPresented: $viewModel.showingAlertConnection) {
                Alert(title: Text("Connection Error"), message: Text("Please try again"), dismissButton: .default(Text("Yes")))
            }
            
            NavigationLink("Navigate To Finish Page", destination: AssesmentFinishView(), isActive: $viewModel.isShowingAssesmentFinishView)
                .hidden()
//                .isDetailLink(false)
            
        }
        .padding(.horizontal, 20)
        .navigationBarTitle(Text("Assessment"), displayMode: .inline)
        .toolbar {
            ToolbarItemGroup(placement: .keyboard) {
                Spacer()
                Button("Done") {
                    keyboardFocus = false
                }
            }
        }
    }
    
    func showResultDurationPicker() {
        let time = String(format: "%02d:%02d:%02d", viewModel.hourDurationSelection, viewModel.minuteDurationSelection, viewModel.secondDurationSelection)
        viewModel.duration = time
        viewModel.showPickerDurationView = false
    }
    
    func showResultPacePicker() {
        let time = String(format: "%02d:%02d", viewModel.minutePaceSelection, viewModel.secondPaceSelection)
        viewModel.pace = time
        viewModel.showPickerPaceView = false
    }
    
    
    func requestPermissionHealtKit() async {
        if !HKHealthStore.isHealthDataAvailable() {
            return
        }
        
        guard await HealthKitUtils().requestPermission() == true else {
            return
        }
    }
}

struct AssesmentLatestRunningView_Previews: PreviewProvider {
    static var previews: some View {
        AssesmentLatestRunningView(assesment: Assesment())
    }
}

//
//  AssesmentLatestRunningDurationView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 07/09/22.
//

import SwiftUI

struct AssesmentLatestRunningDurationView: View {
    @ObservedObject var viewModel: AssesmentLatestRunningViewModel
    
    let hours = [Int](0..<24)
    let minutes = [Int](0..<60)
    let seconds = [Int](0..<60)
    
    var body: some View {
        GeometryReader { geometry in
            HStack(spacing: 0) {
                Picker(selection: self.$viewModel.hourDurationSelection, label: Text("")) {
                    ForEach(0 ..< self.hours.count, id: \.self) { index in
                        Text("\(self.hours[index]) h").tag(index)
                    }
                }
                .pickerStyle(.wheel)
                .frame(width: geometry.size.width/3, height: geometry.size.height, alignment: .center)
                .clipped()
                .compositingGroup()
                
                Picker(selection: self.$viewModel.minuteDurationSelection, label: Text("")) {
                    ForEach(0 ..< self.minutes.count, id: \.self) { index in
                        Text("\(self.minutes[index]) m").tag(index)
                    }
                }
                .pickerStyle(.wheel)
                .frame(width: geometry.size.width/3, height: geometry.size.height, alignment: .center)
                .clipped()
                .compositingGroup()
                
                Picker(selection: self.$viewModel.secondDurationSelection, label: Text("")) {
                    ForEach(0 ..< self.seconds.count, id: \.self) { index in
                        Text("\(self.seconds[index]) s").tag(index)
                    }
                }
                .pickerStyle(.wheel)
                .frame(width: geometry.size.width/3, height: geometry.size.height, alignment: .center)
                .clipped()
                .compositingGroup()
            }
        }
    }
}

struct AssesmentLatestRunningDurationView_Previews: PreviewProvider {
    static var previews: some View {
        AssesmentLatestRunningDurationView(viewModel: AssesmentLatestRunningViewModel())
    }
}

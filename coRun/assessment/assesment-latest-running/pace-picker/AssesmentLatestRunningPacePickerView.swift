//
//  AssesmentLatestRunningPacePickerView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 08/09/22.
//

import SwiftUI

struct AssesmentLatestRunningPacePickerView: View {
    @ObservedObject var viewModel: AssesmentLatestRunningViewModel
    
    let minutes = [Int](0..<60)
    let seconds = [Int](0..<60)
    
    var body: some View {
        GeometryReader { geometry in
            HStack(spacing: 0) {
                Picker(selection: self.$viewModel.minutePaceSelection, label: Text("")) {
                    ForEach(0 ..< self.minutes.count, id: \.self) { index in
                        Text("\(self.minutes[index]) m").tag(index)
                    }
                }
                .pickerStyle(.wheel)
                .frame(width: geometry.size.width/2, height: geometry.size.height, alignment: .center)
                .clipped()
                .compositingGroup()
                
                Picker(selection: self.$viewModel.secondPaceSelection, label: Text("")) {
                    ForEach(0 ..< self.seconds.count, id: \.self) { index in
                        Text("\(self.seconds[index]) s").tag(index)
                    }
                }
                .pickerStyle(.wheel)
                .frame(width: geometry.size.width/2, height: geometry.size.height, alignment: .center)
                .clipped()
                .compositingGroup()
            }
        }
    }
}

struct AssesmentLatestRunningPacePickerView_Previews: PreviewProvider {
    static var previews: some View {
        AssesmentLatestRunningPacePickerView(viewModel: AssesmentLatestRunningViewModel())
    }
}

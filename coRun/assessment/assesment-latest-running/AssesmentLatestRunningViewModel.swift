//
//  AssesmentLatestRunningViewModel.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 07/09/22.
//

import Foundation

class AssesmentLatestRunningViewModel: ObservableObject {
    
    var assesment: Assesment = Assesment()
    
    @Published var isShowingAssesmentFinishView = false
    @Published var showPickerDateView: Bool = false
    @Published var showingAlertConnection = false
    
    @Published var showPickerDurationView: Bool = false
    @Published var hourDurationSelection = 0
    @Published var minuteDurationSelection = 0
    @Published var secondDurationSelection = 0
    
    @Published var showPickerPaceView: Bool = false
    @Published var minutePaceSelection = 0
    @Published var secondPaceSelection = 0
    
    @Published var date = Date()
    @Published var dateFormated: String = ""
    @Published var distance: String = ""
    @Published var pace: String = ""
    @Published var duration: String = ""
    @Published var selectedLocation = "Indoor"
    
    func convertLocation(locatiom: String) -> Bool {
        if locatiom == "Indoor" {
            return true
        } else {
            return false
        }
    }
    
    func convertDate(date: Date) -> String {
        return date.formatted(with: "yyyy-MM-dd")
    }
    
    func postCreateAsessement(assesment: Assesment, token: String, caochID: String) {
        let urlString = "https://mcedevs.com/assessment/create-assessment"
        
        print("UUID \(token)")
        guard let url = URL(string: urlString) else {return}
        
        let body: [String: Any] = ["race_goal": assesment.raceGoal, "running_level": assesment.runningLevel, "available_day": assesment.avaliableDay, "start_training_plan": assesment.raceStart, "coach_id": caochID, "latest_running_date": convertDate(date: self.date), "latest_running_distance": Float(distance) ?? 0.0, "latest_running_duration": duration, "latest_running_pace": "00:\(pace)", "latest_running_indoor": convertLocation(locatiom: selectedLocation)]
        
        print(body)
        
        let finalData = try! JSONSerialization.data(withJSONObject: body)
        
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.httpBody = finalData
        request.setValue(token, forHTTPHeaderField: "coruntoken")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { [self] data, res, error in
            do {
                if let data = data {
                    print("data = " + String(data: data, encoding: .utf8)!)
                    let result = try JSONDecoder().decode(CreateAssesmentResponse.self, from: data)
                    print("result = \(result)")
                    
                    DispatchQueue.main.async { [self] in
                        if result.status_assessment == "pending" {
                            isShowingAssesmentFinishView = true
                        }
                    }
                    
                }
                
            } catch (let error) {
                print("error = \(error) - " +  error.localizedDescription)
                DispatchQueue.main.async {
                    print("Gagal Dong")
                    self.showingAlertConnection = true
                }
            }
        }
        .resume()
    }
}

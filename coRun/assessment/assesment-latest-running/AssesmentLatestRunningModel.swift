//
//  AssesmentLatestRunningModel.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 01/09/22.
//

import Foundation

struct LatestRunning {
    let date: Date
    let distance: Float
    let duration: Timer
    let pace: Timer
    let indor: Bool
}

//
//  Assesment.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 31/08/22.
//

import Foundation

struct Assesment{
    var raceGoal: String = ""
    var runningLevel: String = ""
    var avaliableDay: String = ""
    var raceStart: String = ""
    var activeSheet: ActiveSheet = .assesment
}

struct CreateAssesmentResponse: Codable, Identifiable {
    let status_assessment: String
    let id: Int
}

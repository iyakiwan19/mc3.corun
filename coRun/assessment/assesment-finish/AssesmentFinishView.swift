//
//  AssesmentFinishView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 08/09/22.
//

import SwiftUI

struct AssesmentFinishView: View {
    @State var showAppNavigationView = false
//    @State var assesment: Assesment

    var body: some View {
        VStack{
            Spacer()
            Text("Congratulations!")
                .font(.largeTitle)
                .padding()
            
            HStack {
                Spacer()
                Image(systemName: "checkmark.circle.fill")
                    .font(.system(size: 200))
                    .modifier(ColorModifier.PrimaryGreen())
                Spacer()
            }
            .padding(.vertical, 25)
            
            Text("Your training plan will be ready")
                .font(.largeTitle)
                .padding()
            
            Spacer()
            
            Text("We will inform you when your coach has approved it")
                .multilineTextAlignment(.center)
                .padding(.vertical, 20)
            
            Button(action: {
                showAppNavigationView = true
//                assesment.activeSheet = .finish
            }) {
                Text("Done")
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .font(.system(size: 18))
                    .padding()
                    .foregroundColor(.white)
                    .background(
                        RoundedRectangle(cornerRadius: 10, style: .continuous).fill( Color("ColorButtonActive"))
                    )
            }
            .fullScreenCover(isPresented: $showAppNavigationView) {
                AppNavigationView()
            }
        }
        .padding(.horizontal, 20)
        .navigationBarTitle(Text("Assessment"), displayMode: .inline)
        .navigationBarBackButtonHidden(true)
    }
}

struct AssesmentFinishView_Previews: PreviewProvider {
    static var previews: some View {
        AssesmentFinishView()
    }
}

//
//  AssesmentRunningLevelItemView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 31/08/22.
//

import SwiftUI

struct AssesmentRunningLevelItemView: View {
    let runningLevel: RunningLevel
    let selected: Bool
    var body: some View {
        VStack(spacing: 10) {
            Text(runningLevel.title)
                .font(Font.headline.weight(selected ? .bold: .regular))
            Text(runningLevel.desc)
                .font(.system(size: 14, weight: selected ? .bold: .regular))
                .multilineTextAlignment(.center)
        }
        .padding(.vertical, 15)
        .frame(maxWidth: .infinity)
        .background(Color("colorGray"))
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color("ColorButtonActive"), lineWidth: selected ? 1 : 0)
        )
    }
}

struct AssesmentRunningLevelItemView_Previews: PreviewProvider {
    static var previews: some View {
        AssesmentRunningLevelItemView(runningLevel: dataRunningLevel[0], selected: true)
    }
}

//
//  AssesmentRunningLevelModel.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 31/08/22.
//

import Foundation

struct RunningLevel: Identifiable {
    let id: Int
    let title: String
    let desc: String
}

var dataRunningLevel: [RunningLevel] = [
    .init(id: 0, title: "Beginner", desc: "Just starting to run and able to complete 5km run without stopping"),
    .init(id: 1, title: "Intermediate", desc: "Run regularly and want to starting a training plan"),
    .init(id: 2, title: "Advanced", desc: "Running with structured plan and have finished some race"),
    .init(id: 3, title: "Elite", desc: "Finish some long distance races, have structured training and nutritions")
]

//
//  AssesmentRunningLevelView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 31/08/22.
//

import SwiftUI

struct AssesmentRunningLevelView: View {
    @StateObject var viewModel = AssesmentRunningLevelViewModel()
    @State private var isShowingAssesmentAvaliableDayView = false
    @State var assesment: Assesment

    init(assesment: Assesment) {
        self.assesment = assesment
    }
    
    var body: some View {
        VStack{
            Text("What is your running level?")
                .font(.largeTitle)
                .padding()
            ScrollView(showsIndicators: false) {
                ForEach(dataRunningLevel) { runninglevel in
                    AssesmentRunningLevelItemView(runningLevel: runninglevel, selected: viewModel.selectedRunningLevel == runninglevel.id)
                        .padding(EdgeInsets(top: 10, leading: 20, bottom: 0, trailing: 20))
                        .onTapGesture {
                            viewModel.selectedRunningLevel = runninglevel.id
                            assesment.runningLevel = String(runninglevel.id)
                        }
                }
            }
            Text("2 of 5")
            Button(action: {
                isShowingAssesmentAvaliableDayView = true
            }) {
                Text("Continue")
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .font(.system(size: 18))
                    .padding()
                    .foregroundColor(.white)
                    .background(
                        RoundedRectangle(cornerRadius: 10, style: .continuous).fill(viewModel.selectedRunningLevel >= 0 ? Color("ColorButtonActive") : Color("ColorButtonDisable"))
                    )
            }
            .padding(.horizontal, 20)
            .disabled(viewModel.selectedRunningLevel < 0)
            NavigationLink("Navigate To Avaliable Day", destination: AssesmentAvaliableDayView(assesment: self.assesment), isActive: $isShowingAssesmentAvaliableDayView)
                .hidden()
//                .isDetailLink(false)
        }
        .navigationBarTitle(isShowingAssesmentAvaliableDayView ?  "Back" : "Assessment", displayMode: .inline)
    }
}

struct AssesmentRunningLevelView_Previews: PreviewProvider {
    static var previews: some View {
        AssesmentRunningLevelView(assesment: Assesment())
    }
}

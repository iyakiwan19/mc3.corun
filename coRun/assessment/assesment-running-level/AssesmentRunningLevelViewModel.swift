//
//  AssesmentRunningLevelViewModel.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 31/08/22.
//

import Foundation

class AssesmentRunningLevelViewModel: ObservableObject {
    @Published var selectedRunningLevel: Int = -1
}

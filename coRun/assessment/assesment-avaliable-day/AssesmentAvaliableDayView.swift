//
//  AssesmentAvaliableDayView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 01/09/22.
//

import SwiftUI

struct AssesmentAvaliableDayView: View {
    @StateObject var viewModel = AssesmentAvaliableDayViewModel()
    @State private var isShowingAssesmentRaceStartView = false
    @State var assesment: Assesment

    init(assesment: Assesment) {
        self.assesment = assesment
    }
    
    var body: some View {
        VStack{
            Text("Which days are you able to run?")
                .font(.largeTitle)
                .padding()
                .multilineTextAlignment(.center)
            ScrollView(showsIndicators: false) {
                ForEach(dataAvaliableDay) { avaliableDay in
                    AssesmentAvaliableDayItemView(avaliableDay: avaliableDay.name, selected: viewModel.listAvaliableDay.contains(avaliableDay.id) ? true : false)
                        .padding(EdgeInsets(top: 10, leading: 20, bottom: 0, trailing: 20))
                        .onTapGesture {
                            if(viewModel.checkAvaliableDay(day: avaliableDay.id)) {
                                viewModel.listAvaliableDay.append(avaliableDay.id)
                                viewModel.listAvaliableDay.sort()
                                viewModel.countSelectedAvaliableDay += 1
                            } else {
                                viewModel.listAvaliableDay.removeAll(where: { $0 == avaliableDay.id })
                                viewModel.countSelectedAvaliableDay -= 1
                            }
                            assesment.avaliableDay = viewModel.listAvaliableDay.map { String($0) }.joined(separator: ",")
                            print(assesment.avaliableDay)
                        }
                }
            }
            Text("3 of 5")
            Button(action: {
                isShowingAssesmentRaceStartView = true
            }) {
                Text("Continue")
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .font(.system(size: 18))
                    .padding()
                    .foregroundColor(.white)
                    .background(
                        RoundedRectangle(cornerRadius: 10, style: .continuous).fill(viewModel.countSelectedAvaliableDay > 1 ? Color("ColorButtonActive") : Color("ColorButtonDisable"))
                    )
            }
            .padding(.horizontal, 20)
            .disabled(viewModel.countSelectedAvaliableDay <= 1)
            
            NavigationLink("Navigate To Race Start",destination: AssesmentRaceStartView(assesment: self.assesment), isActive: $isShowingAssesmentRaceStartView)
                .hidden()
//                .isDetailLink(false)
            
        }
        .navigationBarTitle(isShowingAssesmentRaceStartView ?  "Back" : "Assessment", displayMode: .inline)
    }
}

struct AssesmentAvaliableDayView_Previews: PreviewProvider {
    static var previews: some View {
        AssesmentAvaliableDayView(assesment: Assesment())
    }
}

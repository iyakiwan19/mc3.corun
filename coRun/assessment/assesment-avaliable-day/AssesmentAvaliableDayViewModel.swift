//
//  AssesmentAvaliableDayViewModel.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 01/09/22.
//

import Foundation

class AssesmentAvaliableDayViewModel: ObservableObject {
    @Published var countSelectedAvaliableDay: Int = 0
    @Published var listAvaliableDay: [Int] = []
    
    func checkAvaliableDay(day: Int) -> Bool {
        if(listAvaliableDay.contains(day)){
            return false
        }
        return true
    }
}

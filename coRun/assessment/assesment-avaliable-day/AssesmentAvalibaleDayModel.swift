//
//  AssesmentAvalibaleDayModel.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 01/09/22.
//

import Foundation

struct AvaliableDay: Identifiable {
    let id: Int
    let name: String
}

var dataAvaliableDay: [AvaliableDay] = [
    .init(id: 0, name: "Monday"),
    .init(id: 1, name: "Tuesday"),
    .init(id: 2, name: "Wednesday"),
    .init(id: 3, name: "Thursday"),
    .init(id: 4, name: "Friday"),
    .init(id: 5, name: "Saturday"),
    .init(id: 6, name: "Sunday")
]

//
//  AssesmentAvaliableDayItem.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 01/09/22.
//

import SwiftUI

struct AssesmentAvaliableDayItemView: View {
    let avaliableDay: String
    let selected: Bool
    var body: some View {
        Text(avaliableDay)
            .font(.system(size: 18, weight: selected ? .bold: .regular))
            .padding(.vertical, 13)
            .frame(maxWidth: .infinity)
            .background(Color("colorGray"))
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color("ColorButtonActive"), lineWidth: selected ? 1 : 0)
            )
    }
}

struct AssesmentAvaliableDayItem_Previews: PreviewProvider {
    static var previews: some View {
        AssesmentAvaliableDayItemView(avaliableDay: "Monday", selected: true)
    }
}

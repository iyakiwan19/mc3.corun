import Foundation

struct GetTrainingModel: Codable {
    let week: Int?
    let training_plans:[SessionType]?
    
    init(week: Int, training_plans: [SessionType]) {
        self.week = week
        self.training_plans = training_plans
    }
}

struct SessionType: Codable{
    var id : Int?
    var week : Int?
    var day : Int?
    var date : String?
    var trainingType : String?
    var distance : Float?
    var duration : String?
    var pace : String?
    var description : String?
    var status : Int?
    var AssessmentId : Int?
    var createdAt :String?
    var updatedAt :String?
    
    init(id: Int, week: Int, day: Int, date: String, trainingType: String, distance: Float, duration: String, pace: String, description: String, status: Int, createdAt: String, updatedAt: String, AssessmentId: Int) {
        self.id = id
        self.week = week
        self.day = day
        self.date = date
        self.trainingType = trainingType
        self.distance = distance
        self.duration = duration
        self.pace = pace
        self.description = description
        self.status = status
        self.AssessmentId = AssessmentId
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
    
    
    //STATUS DESC
    // 0 => Not DONE
    // 1 => DONE
    // 2 => SKIPPED
    // -1 => DEFAULT DATA
}

struct RunningType: Codable{
    var uuid = ""
    var date = "YYYY-MM-DDT00:00:00.000Z"
    var distance = 0.0
    var duration = "00:00"
    var pace = "0\'0\""
    var indoor = false
}

//
//  ItemWeeklyTrainingCard.swift
//  coRun
//
//  Created by Marselus Richard on 29/08/22.
//

import SwiftUI

struct ItemWeeklyTrainingCard: View {
    var body: some View {
        HStack(spacing:10){
            Circle()
                .foregroundColor(ColorPalette.init().lightGray)
                .frame(width: 20, height: 20)
            VStack{
                Text("Easy Run")
                    .modifier(TextModifier.Body())
                    .modifier(FillToLeftFrame())
                HStack{
                    Text("Monday")
                        .modifier(TextModifier.Caption_1())
                    Text(" - ")
                        .modifier(TextModifier.Caption_1())
                    Text("5 Km")
                        .modifier(TextModifier.Caption_1())
                }.modifier(FillToLeftFrame())
            }
        }
    }
}

struct ItemWeeklyTrainingCard_Previews: PreviewProvider {
    static var previews: some View {
        ItemWeeklyTrainingCard()
    }
}

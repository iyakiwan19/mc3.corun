import SwiftUI

class TutorialSheetModel: ObservableObject {
    let tutorialImage = ["Running1", "Running2", "Running3"]
    let tutorialTitle = [
        "Reach to Your Future Coach!",
        "Fill in the Assessment",
        "Submit and Request your Plan"]
    
    let tutorialDescription = [
        "coRun facilitates you to train and prepare your race, supervised by a coach you prefer.\nWhy? We believe that training with a constant feedback from others would help you progress faster!\nAsk your coach to share their link, then you can proceed to prepare the training!",
        "Tell the coach on your race goal, and your current condition. The training plan will be customized based on your assessment.\nRelax, fill it honestly.\nCause remember, you do you!",
        "We will recommend a suitable training plan for you to your coach. And your coach will be able to customize your plan.\nWe will notify when your plan is coach-customized ready.\nEnjoy your free run in the spare time."]
    
}

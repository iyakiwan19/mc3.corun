import Foundation
import SwiftUI
import SwiftyJSON


enum ActiveSheet: Identifiable {
    case login, inputLink, assesment, finish
    
    var id: Int {
        hashValue
    }
}

class TrainingViewModel:ObservableObject{
    @AppStorage("coruntoken") var coruntoken = ""
    @Published var returnToTrainingView = false
    @Published var showAssesmentView = false
    @Published var activeSheet: ActiveSheet?

    @Published var showingTutorialSheet = false
    @Published var showingAllTrainingPlan = false
    @Published var showingWeeklyTrainingPlanFromTrainingView = false
    @Published var showingWeeklyTrainingPlanFromAllTrainingPlanView = false
    @Published var showingDetailTrainingPlanFromTrainingView = false
    @Published var showingDetailTrainingPlanFromWeekly = false
    @Published var showingSubmissionView =  false
    
    @Published var isTrainingPlanEmpty = true
    
    @Published var currentWeek = 0
    var startDate="YYYY-MM-DD"
    var maxTrainingWeek = 1
    
    var currentDaysOnWeek = 0
    @Published var totalDaysPerWeek = 1
    
    @Published var totalDayClearedThisWeek = 0
    
    @Published var totalDistanceThisWeek = 0.0
    @Published var totalDistance = 0.0
    @Published var avgPace = "0'0\""
    @Published var totalTime = "0m"
    
    var dbTrainingPlan:[SessionType]=[]
    var rawTrainingPlan:[TrainingSession]=[] //TRAINING PLAN DIRECTLY FROM DATABASE
    @Published var bufferTrainingPlan:[TrainingPlan]=[] //TRAINING PLAN THAT HAD BEEN DIVIDED PER WEEK
    @Published var displayTrainingPlan:[TrainingPlan]=[] //ARRAY TRAINING PLAN FOR DISPLAY
    var userActivity:[Running]=[] //User Activity That had been submitted
    
    let flexibleColumns=[
        GridItem(.flexible()),
    ]
    
    func refreshData() async {
        print("Data Refreshed")
    }
    func clearNavigationStatus(){
        showingTutorialSheet = false
        showingAllTrainingPlan = false
        showingWeeklyTrainingPlanFromTrainingView = false
        showingWeeklyTrainingPlanFromAllTrainingPlanView = false
        showingDetailTrainingPlanFromTrainingView = false
        showingDetailTrainingPlanFromWeekly = false
        showingSubmissionView = true
    }
    func clearOverviewData(){
        totalDayClearedThisWeek = 0
        totalDistanceThisWeek = 0.0
        totalDistance = 0.0
        avgPace = "0'0\""
        totalTime = "0"
        
        displayTrainingPlan.removeAll()
        
        //
        //        rawTrainingPlan=[TrainingSession]()
        //        bufferTrainingPlan=[TrainingPlan]()
        //        displayTrainingPlan=[TrainingPlan]()
    }
//    func isTrainingPlanEmpty()->Bool{
//        if(rawTrainingPlan.count==0){
//            //            print("Ja Empty")
//            return true
//        }
//        else{
//            //            print("Nein Empty")
//            return false
//        }
//    }
    
    func GetTrainingPlanFromDatabase(completion:@escaping(Bool)->Void){
        let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXVpZCI6IjExMSIsImlhdCI6MTY2MzE2MTczOH0.kvzN61r_rmWkdAQ181blhlOMKdLnNPa-dk4dgwtawOU"
//        let token = coruntoken
        
        //CODE HERE
        let urlString="https://mcedevs.com/training-plan/get-training-plan"
        
        guard let url=URL(string: urlString)else{return}
        
        var request = URLRequest(url:url)
        request.httpMethod = "GET"
        //request.httpBody = finalData
        request.setValue(token, forHTTPHeaderField: "coruntoken")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        print(">> Request process")
        
        print(request)
        URLSession.shared.dataTask(with: request) { data, res, error in
            do {
                let string = String(data: data!, encoding: .utf8)!
                let data = string.data(using: .utf8)!
                do {
                    if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
                    {
                        for jsonData in jsonArray {
                            let sessionJSON = jsonData["training_plans"] ?? [SessionType]()
                            let dataJSON = JSON(sessionJSON)
                            for sessionJSON in dataJSON {
                                //print(sessionJSON.1["week"].int ?? -1)
                                self.dbTrainingPlan.append(SessionType(
                                    id: sessionJSON.1["id"].int ?? -1,
                                    week: sessionJSON.1["week"].int ?? -1,
                                    day: sessionJSON.1["day"].int ?? -1,
                                    date: sessionJSON.1["date"].string ?? "",
                                    trainingType: sessionJSON.1["trainingType"].string ?? "",
                                    distance: sessionJSON.1["distance"].float ?? 0.0,
                                    duration: sessionJSON.1["duration"].string ?? "",
                                    pace: sessionJSON.1["pace"].string ?? "",
                                    description: sessionJSON.1["description"].string ?? "",
                                    status: sessionJSON.1["status"].int ?? -1,
                                    createdAt: sessionJSON.1["createdAt"].string ?? "",
                                    updatedAt: sessionJSON.1["updatedAt"].string ?? "",
                                    AssessmentId: sessionJSON.1["AssessmentId"].int ?? -1
                                ))
                            }
                        }
                        DispatchQueue.main.async { [self] in
                            print(">> Request Success")
                        }
                    } else {
                        print(">> Bad Json")
                    }
                } catch let error as NSError {
                    print(error)
                }
            } catch (let error) {
                print("error = \(error) - " +  error.localizedDescription)
                DispatchQueue.main.async {
                    print(">> Request Fail")
                    //self.showingAlertConnection = true
                }
            }
            
            print("Data from Database Count: "+String(self.dbTrainingPlan.count))
            for data in self.dbTrainingPlan{
                var newData = TrainingSession()
                newData.day=data.day ?? -1
                newData.week=data.week ?? -1
                newData.trainingType=data.trainingType ?? "default"
                newData.distance=Double(data.distance ?? 0.0)
                newData.duration=data.duration ?? "default"
                newData.pace=data.pace ?? "default"
                newData.description=data.description ?? "default"
                newData.status=data.status ?? -1
                
                newData.date=data.date ?? "2000-01-01"
                newData.createdAt=data.createdAt ?? "2000-01-01"
                newData.updatedAt=data.updatedAt ?? "2000-01-01"
                
                newData.dayName=getDayNameFromDate(dateInYYYYMMDD: data.date ?? "2000-01-01")
                
                self.rawTrainingPlan.append(newData)
            }//LOAD to rawTrainingPlan
            completion(true)
        }
        .resume()
    }//LOAD TRAINING PLAN FROM DATABASE to RawTrainingPlan
    
    func GetUserActivityFromDatabase() async{
        //CODE HERE
    }
    
    func loadBufferTrainingPlan(){
        var iterationCounter=0
        var weekCounter=0
        var newTrainingPlan=TrainingPlan()
        
        print("Raw Count: "+String(rawTrainingPlan.count))
        while(iterationCounter<rawTrainingPlan.count){
            //print(String(weekCounter) + " -- "+String(rawTrainingPlan[iterationCounter].week))
            if(weekCounter==rawTrainingPlan[iterationCounter].week){
                newTrainingPlan.append(newSession: rawTrainingPlan[iterationCounter])
                iterationCounter+=1
            }//APPEND TrainingPlan and GET MaxTrainingWeek
            else{
                bufferTrainingPlan.append(newTrainingPlan)
                newTrainingPlan=TrainingPlan() //Create NEW ARRAY
                weekCounter+=1
            }
        }//DIVIDING TRAINING PLAN by Week
        bufferTrainingPlan.append(newTrainingPlan) //INSERT LATEST ARRAY
        maxTrainingWeek=weekCounter
        
    }// Divide rawTrainingPlan by Week
    func loadDisplayTrainingPlan(filterOption:Int=0){
        //CLEAN DATA
        clearOverviewData()
        
        //  'i' for WEEK and 'j' for DAYS
        for i in 0...maxTrainingWeek-1{
            let totalDay = bufferTrainingPlan[i].session.count
            totalDaysPerWeek=totalDay
            for j in 0...totalDay-1{
                if(bufferTrainingPlan[i].session[j].status==1){
                    currentDaysOnWeek=j+1 //Current Day
                    if(currentDaysOnWeek==totalDaysPerWeek){
                        currentWeek=i+1
                        if(currentWeek>=maxTrainingWeek+1){
                            currentWeek=maxTrainingWeek
                        }
                    }
                    else{
                        currentWeek=i
                    } //Current Week
                }
            }        }
        //  GET CurrentWeek
        //  GET CurrentDaysOnWeek
        //  GET totalDaysPerWeek
        
//        print("Current Week: "+String(currentWeek))
//        print("Max Week: "+String(maxTrainingWeek))
//        print("Current Day on Week: "+String(currentDaysOnWeek))
//        print("Total day per Week: "+String(totalDaysPerWeek))
        
        var totalTimeInSec=0
        var totalPaceInSec=0
        
        for i in 0..<userActivity.count{
            //totalDistance+=Double(userActivity[i].distance ?? 0.0)
            totalDistance+=rawTrainingPlan[i].distance
            
            totalTimeInSec+=convertTimeStringToSecondInt(timeInHHMMSS: userActivity[i].duration ?? "00:00")
            totalPaceInSec+=convertPaceStringToSecondInt(paceInMSS: userActivity[i].pace ?? "0\'0\"")
        }
        // COUNT totalDistance
        // COUNT averagePace
        // COUNT TotalTime
        
        totalTime=convertSecondIntToDisplayString(timeInSecond: totalTimeInSec)
        if(userActivity.count>0){
            avgPace=convertSecondIntToPaceString(timeInSecond: totalPaceInSec/userActivity.count)
        }
        
        bufferTrainingPlan[currentWeek].session.forEach{ data in
            if(data.status==1){
                totalDistanceThisWeek+=Double(data.distance)
                totalDayClearedThisWeek+=1
            }
        }// COUNT THIS WEEK Distance & Cleared Session
        if(filterOption==1){
            if(currentWeek-1 > 0){
                for i in 0...currentWeek-2 {
                    displayTrainingPlan.append(bufferTrainingPlan[i])
                }
            }else{
                displayTrainingPlan.removeAll()
            }
        }// Completed
        else if(filterOption==2){
            for i in currentWeek...maxTrainingWeek-1 {
                displayTrainingPlan.append(bufferTrainingPlan[i])
            }
        }// Not Completed
        else{
            for i in 0...maxTrainingWeek-1 {
                displayTrainingPlan.append(bufferTrainingPlan[i])
            }
        }// All
        
        print("Display Data Updated")
    }// Filter DisplayTrainingPlan & Update Current Progress Information
    // sortOption 0:All 1:Completed 2:NotCompleted
    
    func appendNewUserActivity(newData:Running){
        //CODE HERE
        userActivity.append(newData)
    }
    
    //Create Dummy
    func createDummyTrainingPlan(){
        //print(">> Creating Dummy")
        for week in 1...5{
            //print("  - Week Iteration "+String(week))
            for day in 1...6{
                //print("  - Day Iteration "+String(day))
                
                let newSession=TrainingSession()
                newSession.week=week
                newSession.day=day
                
                newSession.trainingType="Lari Sehat"
                newSession.distance=1.5 * Double(week)
                newSession.duration="00:45:00"
                newSession.description="Lari santuy-santuy sambil smile"
                
                if(week<2){
                    newSession.status=1
                    
                    let newActivity=Running(
                        uuid: String(week+day),
                        date: "YYYY-MM-DDT00:00:00.000Z",
                        distance: Float(1.6 * Double(week)),
                        duration: "01:00:00",
                        pace: "7\'"+String(10+week)+"\"",
                        indoor: false)
                    userActivity.append(newActivity)
                }
                else if(week==2 && day<2){
                    newSession.status=1
                    let newActivity=Running(
                        uuid: String(week+day),
                        date: "YYYY-MM-DDT00:00:00.000Z",
                        distance: Float(1.6 * Double(week)),
                        duration: "00:30:00",
                        pace: "7\'"+String(10*week)+"\"",
                        indoor: false)
                    userActivity.append(newActivity)
                }
                else{
                    newSession.status=0
                }
                rawTrainingPlan.append(newSession)
            }
        }
    }
}

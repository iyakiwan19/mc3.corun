//
//  ViewStyleModifier.swift
//  coRun
//
//  Created by Marselus Richard on 05/09/22.
//

import SwiftUI

class ViewStyleModifier{
    struct StrokeBackgroundModifier:ViewModifier{
        func body(content:Content) -> some View{
            content
                .padding()
                .background(RoundedRectangle(cornerRadius: 10)
                    .strokeBorder(ColorPalette.init().primaryGreen, lineWidth: 1)
                    .background(ColorPalette.init().baseWhite))
                .cornerRadius(10)
        }
    }
    struct CardBackgroundModifier:ViewModifier{
        func body(content:Content) -> some View{
            content
                .padding()
                .background(ColorPalette.init().baseWhite)
                .cornerRadius(10)
        }
    }
//    struct refreshableViewModifier:ViewModifier{
//        func body(content:Content) -> some View{
//            content
//                .onPreferenceChange(ViewOffsetKey.self) {
//                            if $0 < -80 && !isRefreshing {   // << any creteria we want !!
//                                isRefreshing = true
//                                Task {
//                                    await refresh?()           // << call refreshable !!
//                                    await MainActor.run {
//                                        isRefreshing = false
//                                    }
//                                }
//                            }
//                }
//        }
//    }
}

extension Image{
    func CheckedTrainingPlanCheckBoxModifier()->some View{
        self
            .resizable()
            .frame(width: 12, height: 12)
            .foregroundColor(ColorPalette.init().lightGray)
            .background(Circle()
                .strokeBorder(ColorPalette.init().lightGray, lineWidth: 1)
                .background(Circle().fill(ColorPalette.init().baseWhite))
                .frame(width: 24, height: 24))
    }
    func unCheckedTrainingPlanCheckBoxModifier()->some View{
        self
            .resizable()
            .frame(width: 12, height: 12)
            .foregroundColor(ColorPalette.init().baseWhite)
            .background(Circle()
                .strokeBorder(ColorPalette.init().lightGreen, lineWidth: 1)
                .background(Circle().fill(ColorPalette.init().baseWhite))
                .frame(width: 24, height: 24))
    }
}

//
//  AllTrainingView.swift
//  coRun
//
//  Created by Marselus Richard on 05/09/22.
//

import SwiftUI

struct AllTrainingView: View {
    @StateObject var viewModel:TrainingViewModel
    @State var selectedWeekIndex = 1
    @State var showingWeeklyTrainingPlan=false
    @State var filterOption=0
    @State var filterName="All Week"
    
    var body: some View {
        VStack(spacing:20){
            ScrollView{
                HStack{
                    Text("Training Plan")
                        .modifier(TextModifier.Headline())
                        .modifier(FillToLeftFrame())
                        .padding(EdgeInsets(top: 20, leading: 0, bottom: 0, trailing: 0))
                }// Title Section
                
                VStack(spacing:20){
                    NavigationLink(destination: WeeklyTrainingView(viewModel:viewModel,trainingPlan: viewModel.bufferTrainingPlan[selectedWeekIndex]),isActive: $viewModel.showingWeeklyTrainingPlanFromAllTrainingPlanView){EmptyView()}
                    
                    ForEach(viewModel.displayTrainingPlan){data in
                        if(viewModel.currentWeek>=data.session[0].week){
                            AllTrainingPlanCard(viewModel: viewModel,week: data.session[0].week)
                                .onTapGesture {
                                    selectedWeekIndex=data.session[0].week
                                    viewModel.showingWeeklyTrainingPlanFromAllTrainingPlanView=true
                                }
                        }
                        else{
                            AllTrainingPlanCard(viewModel: viewModel,week: data.session[0].week)
                        }// Disable Future Plan
                    }
                }// List of Training Plan
            }.navigationTitle("Your Training")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar{
                    Menu(filterName){
                        Button("All Week"){
                            filterName="All Week"
                            filterOption=0
                            viewModel.loadDisplayTrainingPlan(filterOption: 0)
                        }
                        Button("Completed"){
                            filterName="Completed"
                            filterOption=1
                            viewModel.loadDisplayTrainingPlan(filterOption: 1)
                        }
                        Button("Incomplete"){
                            filterName="Incomplete"
                            filterOption=2
                            viewModel.loadDisplayTrainingPlan(filterOption: 2)
                        }
                    }// Dropdown for Filter
                }
                .padding(EdgeInsets(top: 0, leading: 20, bottom: 20, trailing: 20))
                .onAppear(){
                    viewModel.loadDisplayTrainingPlan(filterOption: filterOption)
                }
            Button("Complete Plan"){
                //Code Here
                //viewModel.clearData()
            }//Complete Plan
            .buttonStyle(ButtonStyleModifier.EnabledFillFrameButton())
            .padding(EdgeInsets(top: 0, leading: 20, bottom: 20, trailing: 20))
        }
    }
}

//struct AllTrainingView_Previews: PreviewProvider {
//    static var previews: some View {
//        AllTrainingView()
//    }
//}

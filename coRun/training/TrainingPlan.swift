//
//  TrainingPlan.swift
//  coRun
//
//  Created by Marselus Richard on 31/08/22.
//

import SwiftUI

class TrainingPlan:ObservableObject, Identifiable{
    @Published var session:[TrainingSession] = []
    @Published var totalDistance=0
    @Published var week = 0
    
    func append(newSession:TrainingSession){
        session.append(newSession)
    }
    func removeAll(){
        session.removeAll()
        totalDistance=0
    }
    
    func getTotalSession()->Int{
        return session.count
    }
    func getTotalDistance()->Double{
        var total=0.0
        session.forEach{ data in
            total+=data.distance
        }
        return total
    }
}

class TrainingSession:ObservableObject,Identifiable{
    //var id = 0
    
    @Published var day = -1
    @Published var week = -1
    @Published var trainingType = "default"
    @Published var distance = 0.0
    @Published var duration = "default"
    @Published var pace = "default"
    @Published var description = "default"
    @Published var status = -1
    
    var date = "YYYY-MM-DDT00:00:00.000Z"
    var createdAt = "YYYY-MM-DDT00:00:00.000Z"
    var updatedAt = "YYYY-MM-DDT00:00:00.000Z"
    
    @Published var dayName = ""
    
    //STATUS DESC
    // 0 => Not DONE
    // 1 => DONE
    // 2 => SKIPPED
    // -1 => DEFAULT DATA
}

//
//  TrainingView.swift
//  coRun
//
//  Created by Marselus Richard on 29/08/22.
//

import SwiftUI

struct EmptyStateTrainingView:View{
    @StateObject var trainingViewModel = TrainingViewModel()

    @AppStorage("uuid") var uuid = ""
    @AppStorage("coachID") var coachID = ""
    var body: some View{
        VStack{
            //                Text("Training Plan")
            //                    .modifier(TextModifier.LargeTitle())
            //                    .modifier(FillToLeftFrame())
            Spacer()
            VStack{
                VStack{
                    Text("You don't have a Training Plan")
                        .modifier(TextModifier.Headline())
                        .multilineTextAlignment(.center)
                        .modifier(ColorModifier.PrimaryGray())
                        .padding(.bottom,10)
                    
                    Text("Training Plan helps you to achieve your goals, improve your running ability, and optimizing your performance. \n Your coach will guide youthrough the entire plan.")
                    .modifier(TextModifier.Body())
                    .multilineTextAlignment(.center)
                    .modifier(ColorModifier.LightGray())
                    .padding(.bottom,10)
                                            
                    Button("Get Training Plan")
                    {
                        //CODE HERE
                        //check if logged in
                        //yes -> Assessment
                        //no -> Login
                        print("User UUID = \(uuid)")
                        if uuid.isEmpty {
                            print("uuid isEmpty, activeSheet = login")
                            trainingViewModel.activeSheet = .login
                        } else {
                            print("coachID isEmpty, activeSheet = inputLink")
                            coachID = ""
                            trainingViewModel.activeSheet = .inputLink
                        }
                    }
                    .buttonStyle(ButtonStyleModifier.PrimaryGreenButton())
                    .fullScreenCover(item: $trainingViewModel.activeSheet, content: { item in
                        switch item {
                        case .login:
                            LoginView()
                                .onDisappear {
                                    if !uuid.isEmpty {
                                        coachID = ""
                                        trainingViewModel.activeSheet = .inputLink
                                    }
                                }
                        case .inputLink:
                            InputLinkView()
                                .onDisappear {
                                    if !coachID.isEmpty {
                                        print("coachID = \(coachID), lanjut ke assesment")
                                        trainingViewModel.activeSheet = .assesment
                                    }
                                }
                        case .assesment:
                            AssesmentCreateView()
                                .onDisappear {
                                    print("AssesmentCreateView onDisappear, lanjut ke training plan")
                                }
                        default:
                            EmptyView()
                       }
                    })
                }
                .padding(25)
                .background(ColorPalette.init().baseWhite)
                .cornerRadius(10)
                
                Text("How do i get Training Plan?")
                    .underline()
                    .modifier(TextModifier.SubHead())
                    .modifier(ColorModifier.PrimaryGreen())
                    .onTapGesture {
                        //TUTORIAL POP UP HERE
                        trainingViewModel.showingTutorialSheet.toggle()
                    }
                    .sheet(isPresented: $trainingViewModel.showingTutorialSheet) {
                                TutorialSheet()
                            }
            }
            Spacer()
        }.modifier(FillFrame())
            .padding(EdgeInsets(top: 25, leading: 25, bottom: 50, trailing: 25))
            .refreshable {
                await trainingViewModel.refreshData()
            }
    }
}

struct ItemStateTrainingView:View{
    @ObservedObject var viewModel : TrainingViewModel
    @State var selectedDayIndex=0
    @State var refreshProgressBar = false
    
    var body: some View{
        VStack(spacing:20){
            HStack(spacing:0){
                VStack(spacing:5){
                    HStack(spacing:0){
                        Text(String(Int(viewModel.totalDistance)))
                            .modifier(TextModifier.Title_1())
                            .modifier(ColorModifier.PrimaryGreen())
                        Text(" Km")
                            .modifier(TextModifier.Title_1())
                            .modifier(ColorModifier.PrimaryGreen())
                    }
                    Text("Distance")
                        .modifier(TextModifier.Caption_1())
                }.modifier(FillFrame())
                VStack(spacing:5){
                    Text(viewModel.avgPace)
                        .modifier(TextModifier.Title_1())
                        .modifier(ColorModifier.PrimaryGreen())
                    Text("Avg. Pace")
                        .modifier(TextModifier.Caption_1())
                }.modifier(FillFrame())
                VStack(spacing:5){
                    Text(String(viewModel.totalTime))
                        .modifier(TextModifier.Title_1())
                        .modifier(ColorModifier.PrimaryGreen())
                    Text("Time")
                        .modifier(TextModifier.Caption_1())
                }.modifier(FillFrame())
            }//Training Overview
            .padding(20)
            .background(ColorPalette.init().baseWhite)
            .cornerRadius(10)
            
            VStack(spacing:20){
                Group{
                    NavigationLink(destination: WeeklyTrainingView(viewModel:viewModel,trainingPlan:viewModel.bufferTrainingPlan[viewModel.currentWeek]), isActive: $viewModel.showingWeeklyTrainingPlanFromTrainingView){EmptyView()}
                    HStack{
                        HStack(spacing:0){
                            Group{
                                Text("Week ")
                                Text(String(viewModel.currentWeek+1))
                                Text(" Progress")
                            }.modifier(TextModifier.Headline())
                        }
                        Spacer()
                        HStack(spacing:0){
                            Group{
                                Text(String(viewModel.totalDayClearedThisWeek))
                                Text(String(" of "))
                                Text(String(viewModel.totalDaysPerWeek))
                                Text(String(" days"))
                                Image(systemName: "chevron.right")
                                    .modifier(ColorModifier.PrimaryGray())
                            }.modifier(TextModifier.Body())
                        }
                    }//Title Section
                    
                    if(refreshProgressBar==false || refreshProgressBar==true){
                        HStack(spacing:5){
                            ForEach(0..<viewModel.totalDaysPerWeek){ index in
                                if(index<viewModel.totalDayClearedThisWeek){
                                    Rectangle()
                                        .modifier(ColorModifier.PrimaryGreen())
                                        .modifier(FillFrame())
                                        .frame(height:6)
                                        .cornerRadius(5)
                                }
                                else{
                                    Rectangle()
                                        .foregroundColor(ColorPalette.init().lightGray)
                                        .modifier(FillFrame())
                                        .frame(height:6)
                                        .cornerRadius(5)
                                }
                            }
                            
                        }//Progress Bar
                    }
                    
                    HStack{
                        Text("Total Kilometers")
                            .modifier(TextModifier.Headline())
                        Spacer()
                        HStack(spacing:0){
                            Group{
                                Text(String(viewModel.totalDistanceThisWeek))
                                Text(" Km")
                            }.modifier(TextModifier.Body())
                        }
                    }// Total Kilometers Section
                }// Navigate to Weekly Training Plan
                .onTapGesture{
                    viewModel.showingWeeklyTrainingPlanFromTrainingView=true
                }
                
                VStack(spacing:5){
                    ForEach(viewModel.bufferTrainingPlan[viewModel.currentWeek].session){data in
                        Divider()
                        itemListTrainingPlanCard(viewModel:data)
                            .padding(EdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 0))
                    }
                }// List of Training Plan
            }//Training Plan Progress Card
            .modifier(ViewStyleModifier.StrokeBackgroundModifier())
            
            Group{
                NavigationLink(destination: AllTrainingView(viewModel: viewModel), isActive: $viewModel.showingAllTrainingPlan) { EmptyView() }
                Text("Show All")
                    .modifier(TextModifier.Body())
                    .modifier(ColorModifier.PrimaryGreen())
                    .onTapGesture{
                        viewModel.showingAllTrainingPlan=true
                    }
            }// Navigate To Show All Training Plan
        }.padding(EdgeInsets(top: 25, leading: 25, bottom: 50, trailing: 25))
            .onAppear(){
                refreshProgressBar.toggle()
                print(" >> refreshProgressBar: \(refreshProgressBar)")
                print(" >> tDCtW/tDpW : \(viewModel.totalDayClearedThisWeek)/\(viewModel.totalDaysPerWeek)")
            }
    }
}

struct TrainingView: View {
    @StateObject var viewModel=TrainingViewModel()
    @State var selectedIndex = -1
    @AppStorage("uuid") var uuid = ""
    @State var showLogin = false
    
    var body: some View {
        VStack{
            if !uuid.isEmpty {
                if(viewModel.isTrainingPlanEmpty){
                    EmptyStateTrainingView(trainingViewModel: viewModel)
                }else{
                    ItemStateTrainingView(viewModel: viewModel)
                }
            } else {
                EmptyStateTrainingView(trainingViewModel: viewModel)
            }
        }
        .onAppear(){
            if !uuid.isEmpty {
                if(viewModel.isTrainingPlanEmpty){
                    //viewModel.createDummyTrainingPlan()
                    
                    viewModel.GetTrainingPlanFromDatabase(){status in
                        if(viewModel.rawTrainingPlan.count>0){
                            viewModel.loadBufferTrainingPlan()
                            viewModel.loadDisplayTrainingPlan()
                            viewModel.isTrainingPlanEmpty=false
                            
                            viewModel.objectWillChange.send()
                        }
                    }
                }// CHECK IF TrainingPlan ALREADY LOADED
                else{
                    viewModel.loadDisplayTrainingPlan()
                }
//            } else {
//                showLogin = true
            }
        }
        .fullScreenCover(isPresented: $showLogin) {
            LoginView()
        }
    }
}

struct TrainingView_Previews: PreviewProvider {
    static var previews: some View {
        TrainingView()
    }
}

//
//  TrainingPlanCard.swift
//  coRun
//
//  Created by Marselus Richard on 05/09/22.
//

import SwiftUI

struct itemListTrainingPlanCard: View {
    @StateObject var viewModel:TrainingSession
    var body: some View {
        HStack(alignment:.center){
            VStack(alignment:.leading,spacing:5){
                Text(viewModel.trainingType)
                    .modifier(TextModifier.Body())
                HStack(spacing:0){
                    Group{
                        Text(convertDayNumberToName(dayNumber:viewModel.day))
                        Text(" - ")
                        Text(String(viewModel.distance))
                        Text(" Km")
                        
                    }.modifier(TextModifier.Footnote())
                }
            }
            Spacer()
            if(viewModel.status==1){
                Text("Completed")
                    .modifier(TextModifier.Footnote())
                    .modifier(ColorModifier.PrimaryGray())
            }
//            Image(systemName: "chevron.right")
//                .modifier(TextModifier.Body())
//                .modifier(ColorModifier.PrimaryGray())
        }.padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
    }
}

struct AllTrainingPlanCard:View{
    @StateObject var viewModel:TrainingViewModel
    @State var week:Int
    
    var body: some View{
        if(week<viewModel.currentWeek){
            HStack{
                HStack(spacing:0){
                    Group{
                        Text("Week ")
                        Text(String(week+1))
                    }.modifier(TextModifier.Headline())
                }
                Spacer()
                Text("Completed")
                    .modifier(TextModifier.Footnote())
                    .modifier(ColorModifier.PrimaryGray())
                Image(systemName: "chevron.right")
                    .modifier(TextModifier.Body())
                    .modifier(ColorModifier.PrimaryGray())
//                Image(systemName: "checkmark")
//                    .CheckedTrainingPlanCheckBoxModifier()
            }.modifier(ViewStyleModifier.CardBackgroundModifier())
        }
        else if(week>viewModel.currentWeek){
            HStack{
                HStack(spacing:0){
                    Group{
                        Text("Week ")
                        Text(String(week+1))
                    }.modifier(TextModifier.Headline())
                }
                Spacer()
                Image(systemName: "chevron.right")
                    .modifier(TextModifier.Body())
                    .modifier(ColorModifier.PrimaryGray())
//                Image(systemName: "checkmark")
//                    .unCheckedTrainingPlanCheckBoxModifier()
            }.modifier(ViewStyleModifier.CardBackgroundModifier())
        }
        else{
            VStack(spacing:20){
                HStack{
                    HStack(spacing:0){
                        Group{
                            Text("Week ")
                            Text(String(viewModel.currentWeek+1))
                            Text(" Progress")
                        }.modifier(TextModifier.Headline())
                    }
                    Spacer()
                    HStack(spacing:0){
                        Group{
                            Text(String(viewModel.totalDayClearedThisWeek))
                            Text(String(" of "))
                            Text(String(viewModel.totalDaysPerWeek))
                            Text(String(" days"))
                            Image(systemName: "chevron.right")
                                .modifier(ColorModifier.PrimaryGray())
                        }.modifier(TextModifier.Body())
                    }
                }//Title Section
                
                HStack(spacing:5){
                    ForEach(0..<viewModel.totalDaysPerWeek){ index in
                        if(index<viewModel.totalDayClearedThisWeek){
                            Rectangle()
                                .modifier(ColorModifier.PrimaryGreen())
                                .modifier(FillFrame())
                                .frame(height:6)
                                .cornerRadius(5)
                        }
                        else{
                            Rectangle()
                                .foregroundColor(ColorPalette.init().lightGray)
                                .modifier(FillFrame())
                                .frame(height:6)
                                .cornerRadius(5)
                        }
                    }
                }//Progress Bar
                
                HStack{
                    Text("Total Kilometers")
                        .modifier(TextModifier.Headline())
                    Spacer()
                    HStack(spacing:0){
                        Group{
                            Text(String(viewModel.totalDistanceThisWeek))
                                Text(" Km")
                        }.modifier(TextModifier.Body())
                    }
                }
            }.modifier(ViewStyleModifier.StrokeBackgroundModifier())
        }
    }
}

struct WeeklyTrainingPlanCard:View{
    @StateObject var trainingSession:TrainingSession
    
    var body: some View{
        if(trainingSession.status==1){
            HStack{
                VStack(alignment:.leading,spacing:5){
                    Text(trainingSession.trainingType)
                        .modifier(TextModifier.Body())
                    HStack(spacing:0){
                        Group{
                            Text(convertDayNumberToName(dayNumber:trainingSession.day))
                            Text(" - ")
                            Text(String(trainingSession.distance))
                            Text(" Km")
                            
                        }.modifier(TextModifier.Footnote())
                    }
                }
                Spacer()
                Text("Completed")
                    .modifier(TextModifier.Footnote())
                    .modifier(ColorModifier.PrimaryGray())
                Image(systemName: "chevron.right")
                    .modifier(TextModifier.Body())
                    .modifier(ColorModifier.PrimaryGray())
            }.modifier(ViewStyleModifier.CardBackgroundModifier())
        }
        else if(trainingSession.status==0){
            HStack{
                VStack(alignment:.leading,spacing:5){
                    Text(trainingSession.trainingType)
                        .modifier(TextModifier.Body())
                    HStack(spacing:0){
                        Group{
                            Text(convertDayNumberToName(dayNumber:trainingSession.day))
                            Text(" - ")
                            Text(String(trainingSession.distance))
                            Text(" Km")
                            
                        }.modifier(TextModifier.Footnote())
                    }
                }
                Spacer()
                Image(systemName: "chevron.right")
                    .modifier(TextModifier.Body())
                    .modifier(ColorModifier.PrimaryGray())
            }.modifier(ViewStyleModifier.StrokeBackgroundModifier())
        }
        else{
            HStack{
                VStack(alignment:.leading,spacing:5){
                    Text(trainingSession.trainingType)
                        .modifier(TextModifier.Body())
                    HStack(spacing:0){
                        Group{
                            Text(convertDayNumberToName(dayNumber:trainingSession.day))
                            Text(" - ")
                            Text(String(trainingSession.distance))
                            Text(" Km")
                            
                        }.modifier(TextModifier.Footnote())
                    }
                }
                Spacer()
                Image(systemName: "chevron.right")
                    .modifier(TextModifier.Body())
                    .modifier(ColorModifier.PrimaryGray())
            }.modifier(ViewStyleModifier.StrokeBackgroundModifier())
        }
    }
}

//
//  WeeklyTrainingView.swift
//  coRun
//
//  Created by Marselus Richard on 05/09/22.
//

import SwiftUI

struct WeeklyTrainingView: View {
    @ObservedObject var viewModel:TrainingViewModel
    @ObservedObject var trainingPlan : TrainingPlan
    @State var selectedIndex=0
    
    var body: some View {
        VStack{
            ScrollView{
                VStack(spacing:20){
                    HStack(spacing:0){
                        Group{
                            Text("Week ")
                            Text(String(trainingPlan.session[0].week+1))
                            Text(" Details")
                        }.modifier(TextModifier.Headline())
                    }//Title Section
                    .modifier(FillToLeftFrame())
                    .padding(EdgeInsets(top: 20, leading: 0, bottom: 0, trailing: 0))

                    VStack(spacing:20){
//                        ForEach(trainingPlan.session){ session in
//                            WeeklyTrainingPlanCard(trainingSession: session)
//                                .onTapGesture {
//                                    selectedIndex=session.day
//                                    viewModel.showingDetailTrainingPlanFromWeekly=true
//                                }
//                        }
                        ForEach(trainingPlan.session.indices){ index in
                            WeeklyTrainingPlanCard(trainingSession: trainingPlan.session[index])
                                .onTapGesture {
                                    selectedIndex=index
                                    viewModel.showingDetailTrainingPlanFromWeekly=true
                                }
                        }
                    }// List week ofTrainingPlan
                    .background(){
                        NavigationLink(destination:DetailTrainingView(viewModel:viewModel,trainingSession: trainingPlan.session[selectedIndex]),isActive: $viewModel.showingDetailTrainingPlanFromWeekly){EmptyView()}
                    }
                    
//                    if(){
//                        Button("Complete Week"){
//                            //Code Here
//                        }.buttonStyle(ButtonStyleModifier.EnabledFillFrameButton())
//                    }
//                    else{
//                        
//                    }
                }
            }
        }
        .navigationTitle("Week "+String(trainingPlan.session[0].week+1))
        .navigationBarTitleDisplayMode(.inline)
        .padding(EdgeInsets(top: 0, leading: 20, bottom: 20, trailing: 20))
    }
}

//struct WeeklyTrainingView_Previews: PreviewProvider {
//    static var previews: some View {
//        WeeklyTrainingView()
//    }
//}

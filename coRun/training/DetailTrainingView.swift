//
//  DetailTrainingView.swift
//  coRun
//
//  Created by Marselus Richard on 05/09/22.
//

import SwiftUI

struct DetailTrainingView: View {
    @StateObject var viewModel:TrainingViewModel
    @StateObject var trainingSession:TrainingSession
    
    var body: some View {
        VStack{
            NavigationLink(destination: SubmissionView(trainingViewModel: viewModel,trainingSession: trainingSession), isActive: $viewModel.showingSubmissionView){EmptyView()}
            
            Text("Activity Details")
                .modifier(TextModifier.Title_3())
                .modifier(FillToLeftFrame())
                .padding(20)
            Divider()
            VStack(alignment:.leading, spacing:20){
                HStack{
                    Group{
                        Image(systemName: "calendar")
                        Text(convertDayNumberToName(dayNumber:trainingSession.day))
                    }.modifier(TextModifier.Headline())
                }// Day
                HStack{
                    Group{
                        Image(systemName: "hare")
                        Text(String(trainingSession.trainingType))
                    }.modifier(TextModifier.Headline())
                }// Type
                HStack{
                    Group{
                        Image(systemName: "ruler")
                        Text(String(trainingSession.distance)+" Km")
                    }.modifier(TextModifier.Headline())
                }// Distance
                if(trainingSession.status==1){
                    Button("Done"){
                    }.buttonStyle(ButtonStyleModifier.DisabledFillFrameButton())
                }
                else{
                    Button("Submit Data"){
                        //trainingSession.status=1
                        //CODE HERE
                        viewModel.showingSubmissionView = true
                    }.buttonStyle(ButtonStyleModifier.EnabledFillFrameButton())
                }
            }// Detail Section
            .padding(20)
            Divider()
            VStack(spacing:20){
                Group{
                    Text("Description")
                        .modifier(TextModifier.Headline())
                    Text(trainingSession.description)
                }.modifier(FillToLeftFrame())
            }// Description Section
            .padding(20)
            Spacer()
        }
        .navigationTitle("Your Training")
        .navigationBarTitleDisplayMode(.inline)
    }
}

//struct DetailTrainingView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailTrainingView()
//    }
//}

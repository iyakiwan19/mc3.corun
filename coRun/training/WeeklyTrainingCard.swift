//
//  WeeklyTrainingCard.swift
//  coRun
//
//  Created by Marselus Richard on 29/08/22.
//

import SwiftUI

struct WeeklyTrainingCard: View {
    var body: some View {
        VStack{
            HStack{
                Text("Weekly Kilometers")
                    .modifier(TextModifier.Headline())
                    .modifier(FillToLeftFrame())
                Text("41 Km")
            }
            ScrollView{
                ItemWeeklyTrainingCard()
                    .modifier(FillFrame())
                ItemWeeklyTrainingCard()
                    .modifier(FillFrame())
                ItemWeeklyTrainingCard()
                    .modifier(FillFrame())
                ItemWeeklyTrainingCard()
                    .modifier(FillFrame())
            }
            .listStyle(PlainListStyle())
        }
        .padding(20)
        .background(ColorPalette.init().baseWhite)
        .cornerRadius(10)
    }
}

struct WeeklyTrainingCard_Previews: PreviewProvider {
    static var previews: some View {
        WeeklyTrainingCard()
    }
}

//
//  TutorialSheet.swift
//  coRun
//
//  Created by Marselus Richard on 30/08/22.
//

import SwiftUI

struct TutorialSheet: View {
    @Environment(\.dismiss) var dismiss
    @StateObject var viewModel=TutorialSheetModel()
    
        var body: some View {
            NavigationView{
                TabView{
                    ForEach(0..<3){index in
                        VStack(spacing:20){
                            Image(viewModel.tutorialImage[index])
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(minWidth:0,maxWidth:.infinity,maxHeight: 360)
                                .clipped()
                                .cornerRadius(10)
                            Text(viewModel.tutorialTitle[index])
                                .modifier(TextModifier.Headline())
                                .multilineTextAlignment(.center)
                            Text(viewModel.tutorialDescription[index])
                                .modifier(TextModifier.Body())
                                .multilineTextAlignment(.center)
//                            Button("Dismiss"){
//                                dismiss()
//                            }.buttonStyle(ButtonStyleModifier.PrimaryGreenButton())
                            Spacer()
                        }.padding(EdgeInsets(top: 25, leading: 25, bottom: 50, trailing: 25))
                    }
                }
                .navigationTitle("How do i get Training Plan")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar{
                    Button{
                        dismiss()
                    }label: {
                        Image(systemName: "xmark")
                            .modifier(ColorModifier.textColor())
                    }
                }
                .tabViewStyle(.page)
                .indexViewStyle(.page(backgroundDisplayMode: .always))
            }
        }
}

struct TutorialSheet_Previews: PreviewProvider {
    static var previews: some View {
        TutorialSheet()
    }
}

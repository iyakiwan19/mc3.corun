//
//  TimeHandler.swift
//  coRun
//
//  Created by Marselus Richard on 08/09/22.
//

import SwiftUI

func convertDayNumberToName(dayNumber:Int)->String{
    switch(dayNumber){
    case 0:
        return "Monday"
    case 1:
        return "Tuesday"
    case 2:
        return "Wednesday"
    case 3:
        return "Thursday"
    case 4:
        return "Friday"
    case 5:
        return "Saturday"
    case 6:
        return "Sunday"
    default:
        return "-- "
    }
}

func convertPaceStringToSecondInt(paceInMSS:String)->Int{
    var result = 0
    var splittedString = ""
    
    for char in paceInMSS{
        if(char==":"){
            result+=(Int(splittedString) ?? 0) * 60
            splittedString=""
        }
        else{
            splittedString+=String(char)
        }
        
//        if(char=="\'"){
//            result+=(Int(splittedString) ?? 0) * 60
//            splittedString=""
//        }
//        else if(char=="\""){
//            result+=(Int(splittedString) ?? 0)
//            splittedString=""
//        }
//        else{
//            splittedString+=String(char)
//        }
    }
    result+=(Int(splittedString) ?? 0)
    splittedString=""
    return result
}

func convertSecondIntToPaceString(timeInSecond:Int)->String{
    var timeInString = ""
    var timeCount = 0
    var timeLeft = timeInSecond
    var divider = 60
    
    for i in 0...1{
        timeCount=timeLeft / divider
        timeLeft=timeLeft % divider
        divider/=60

        timeInString+=String(timeCount)
        
        switch(i){
        case 0:
            timeInString+="\'"
        case 1:
            timeInString+="\""
        default:
            timeInString+="-"
        }
    }
    return timeInString
}

func convertTimeStringToSecondInt(timeInHHMMSS:String)->Int{
    if(timeInHHMMSS.isEmpty){
        return 0
    }else{
        var result = 0
        let splitedTime = timeInHHMMSS.components(separatedBy: ":")
        var multiplication=1
        
        for i in (0...splitedTime.count-1).reversed(){
            result+=(Int(splitedTime[i]) ?? 0)  * multiplication
            multiplication*=60
        }
        return result
    }
} // HH:MM:SS to Int(second)

func convertSecondIntToStringHHMMSS(timeInSecond:Int)->String{
    var timeInString = ""
    var timeCount = 0
    var timeLeft = timeInSecond
    var divider = 3600
    
    for i in 0...2{
        timeCount=timeLeft / divider
        timeLeft=timeLeft % divider
        divider/=60

        if(timeCount/10==0){
            timeInString+="0"+String(timeCount)
        }
        else{
            timeInString+=String(timeCount)
        }
        
        if(!(i==2)){
            timeInString+=":"
        }
    }
    return timeInString
}// Int(Second) to HH:MM:SS

func convertSecondIntToDisplayString(timeInSecond:Int)->String{
    if(timeInSecond==0){
        return "0m"
    }
    
    var timeInString = ""
    var timeCount = 0
    var timeLeft = timeInSecond
    var divider = 3600
    
    for i in 0...1{
        timeCount=timeLeft / divider
        timeLeft=timeLeft % divider
        divider/=60

        if(!(timeCount==0)){
            timeInString+=String(timeCount)
            switch(i){
            case 0:
                timeInString+="h"
            case 1:
                timeInString+="m"
            default:
                timeInString+="x"
            }
            
        }
    }
    return timeInString
} // Int(Second) to xhym

func reformatYYYYMMDDtoDDMMYYYYStringFormat(dateInYYYYMMDD:String)->String{
    var year = ""
    var month = ""
    var day = ""
    
    var i=0
    
    for char in dateInYYYYMMDD{
        if(char=="-"){
            i+=1
        }
        else{
            if(i==0){
                year+=String(char)
            }else if(i==1){
                month+=String(char)
            }
            else if(i==2){
                day+=String(char)
            }
        }
    }
    return day+"/"+month+"/"+year
}

func getDayNameFromDate(dateInYYYYMMDD:String)->String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy'-'MM'-'dd'"
    let date = dateFormatter.date(from: dateInYYYYMMDD)
    dateFormatter.dateFormat="EEEE"

    return dateFormatter.string(from: date ?? Date())
}

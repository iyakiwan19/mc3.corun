//
//  InputLink.swift
//  coRun
//
//  Created by Ricky Suprayudi on 07/09/22.
//

import Foundation

struct InputLink: Codable, Identifiable {
    let id: Int
    let uuid: String
    let first_name: String
    let last_name: String
    let sex: Int?
    let birth_date: String?
    let height: Int?
    let weight: Int?
    let photo: String?
    let status: String?
    let createdAt: String?
    let updatedAt: String?
}

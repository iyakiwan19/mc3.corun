//
//  InputLinkViewModel.swift
//  coRun
//
//  Created by Ricky Suprayudi on 05/09/22.
//

import Foundation
import Combine

class InputLinkViewModel: ObservableObject {
    @Published var coachID = ""
    @Published var fullName = ""
    @Published var coachNotFound = false
    @Published var showPickCoachView = false
    @Published var loginFirst = false
    
    func processLink(link: String, coRunToken: String) {
        showPickCoachView = false
        coachNotFound = false
        fullName = ""
        coachID = link
        
        print("Processing \(link) using token: \(coRunToken)")
        
        let urlString = "https://mcedevs.com/user/get-data-user/\(link)"
        
        guard let url = URL(string: urlString) else { return }
        
        var request = URLRequest(url: url)
        
        request.httpMethod = "GET"
        
        request.setValue(coRunToken, forHTTPHeaderField: "coruntoken")
        
        URLSession.shared.dataTask(with: request) { [self] data, res, error in
                   do {
                       if let data = data {
                           print("data = " + String(data: data, encoding: .utf8)!)
                           let result = try JSONDecoder().decode(InputLink.self, from: data)
                           print("result = \(result)")

                           DispatchQueue.main.async { [self] in
                               fullName = "\(result.first_name) \(result.last_name)"
                               showPickCoachView = true
                               coachNotFound = false
                           }

                       }
                       
                   } catch (let error) {
                       print("error = \(error) - " +  error.localizedDescription)
                       DispatchQueue.main.async {
                           self.coachNotFound = true
                       }
                   }
               }
               .resume()
    }
}

//
//  InputLinkView.swift
//  coRun
//
//  Created by Ricky Suprayudi on 01/09/22.
//

import SwiftUI

struct InputLinkView: View {
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.colorScheme) var currentMode
    @FocusState var keyboardFocus: Bool
    @State private var shouldAnimate = false
    @State var coachLink = ""
    @State var currentDate = Date()
    @State var submitLinkButtonIsDisabled = true
    @StateObject var inputLinkViewModel = InputLinkViewModel()
    @AppStorage("coruntoken") var coruntoken = ""
    @AppStorage("coachID") var coachID = ""
    
    var textFieldBackground: some View {
        RoundedRectangle(cornerRadius: 10)
            .fill(currentMode == .dark ? Color(UIColor.systemBackground) : Color("textFieldBackground"))
    }
    var textFieldBorder: some View {
        RoundedRectangle(cornerRadius: 10)
            .stroke(lineWidth: 1)
            .fill(Color("textFieldBorder"))
    }
    
    var body: some View {
        NavigationView {
            VStack {
                Text("Get a link from your\npreferred coach")
                    .multilineTextAlignment(.center)
                    .modifier(TextModifier.LargeTitle())
                    .padding(.top, 40)
                    .padding(.bottom, 80)
                HStack {
                    Text("Coach Link")
                        .modifier(TextModifier.Headline())
                        .padding(.horizontal, 25)
                    Spacer()
                }
                HStack {
                    HStack {
                        TextField("Type here..", text: $coachLink)
                        .textInputAutocapitalization(.never)
                        .focused($keyboardFocus)
                        .disableAutocorrection(true)
                        .modifier(TextModifier.Body())
                        .padding(.horizontal, 15)
                        .padding(.vertical, 20)
                        .background(textFieldBackground)
                        .overlay(textFieldBorder)
                        .onChange(of: coachLink) { newValue in
                            if coachLink.count > 0 {
                                submitLinkButtonIsDisabled = false
                                inputLinkViewModel.coachNotFound = false
                                inputLinkViewModel.loginFirst = false
                                shouldAnimate = false
                            } else {
                                submitLinkButtonIsDisabled = true
                            }
                            
                        }
                        .onTapGesture {
                            submitLinkButtonIsDisabled = false
                            inputLinkViewModel.coachNotFound = false
                            inputLinkViewModel.loginFirst = false
                            shouldAnimate = false
                        }
                    }
                    HStack {
                        Button {
                            keyboardFocus = false
                            if !coruntoken.isEmpty {
                                shouldAnimate = true
                                submitLinkButtonIsDisabled = true
                                inputLinkViewModel.processLink(link: coachLink, coRunToken: coruntoken)
                                print("fullName = \(inputLinkViewModel.fullName)")
                                coachID = ""
                            } else {
                                print("please login first.")
                                inputLinkViewModel.loginFirst = true
                            }
                        } label: {
                            if shouldAnimate && !inputLinkViewModel.coachNotFound {
                                ActivityIndicator()
                            } else {
                                Image(systemName: "chevron.forward")
                                    .modifier(TextModifier.Title_1())
                            }
                        }
                        .buttonStyle(ButtonStyleModifier.SubmitCoachLinkButton(disabled: submitLinkButtonIsDisabled))
                        .disabled(submitLinkButtonIsDisabled)
                        .fullScreenCover(isPresented: $inputLinkViewModel.showPickCoachView, onDismiss: {
                            shouldAnimate = false
                            submitLinkButtonIsDisabled = false
                            if !coachID.isEmpty {
                                 presentationMode.wrappedValue.dismiss()
                                // masuk ke assesment
                            }
                        }) {
                            PickCoachView(coachUUID: "\(inputLinkViewModel.coachID)", coachName: "\(inputLinkViewModel.fullName)")
                        }
                    }
                    .padding(.leading, 20)
                }
                .padding(.horizontal, 25)
                if inputLinkViewModel.coachNotFound {
                    HStack {
                        Text("Coach not found.")
                            .modifier(TextModifier.Footnote())
                            .foregroundColor(.red)
                        Spacer()
                    }
                    .padding(.leading, 25)
                }
                if inputLinkViewModel.loginFirst {
                    HStack {
                        Text("Please login first.")
                            .modifier(TextModifier.Footnote())
                            .foregroundColor(.red)
                        Spacer()
                    }
                    .padding(.leading, 25)
                }
                Spacer()
            }
            .toolbar(content: {
                ToolbarItem(placement: .primaryAction) {
                    Button {
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        Image(systemName: "xmark")
                            .modifier(TextModifier.Headline())
                            .foregroundColor(.red)
                    }
                }
                ToolbarItem(placement: .principal) {
                    Text("Pick Coach")
                        .modifier(TextModifier.Headline())
                }
                ToolbarItemGroup(placement: .keyboard) {
                    Button("Clear") {
                        coachLink = ""
                    }
                    Spacer()
                    Button("Done") {
                        keyboardFocus = false
                    }
                }
            })
        }
    }
}

struct InputLinkView_Previews: PreviewProvider {
    static var previews: some View {
        InputLinkView()
    }
}

struct ActivityIndicator: UIViewRepresentable {
//    @Binding var shouldAnimate: Bool
    func makeUIView(context: Context) -> UIActivityIndicatorView {
        // Create UIActivityIndicatorView
        return UIActivityIndicatorView()
    }

    func updateUIView(_ uiView: UIActivityIndicatorView, context: Context) {
        // Start and stop UIActivityIndicatorView animation
//        if self.shouldAnimate {
            uiView.startAnimating()
//        } else {
//            uiView.stopAnimating()
//        }
    }
    
}

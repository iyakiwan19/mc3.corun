//
//  deepLinkModel.swift
//  coRun
//
//  Created by Ricky Suprayudi on 12/09/22.
//

import Foundation
import Combine

class DeepLinkModel: ObservableObject {
    
    @Published var coachID = ""

    func checkDeepLink(url: URL) -> Bool {

        guard let host = URLComponents(url: url, resolvingAgainstBaseURL: true)?.host else {
            return false
        }
        
        let page = host.components(separatedBy: "=")
        print("pages = \(page[0]) and \(page[1])")
        if page[0] == "coach" {
            coachID = page[1]
            return true
        }

        return false
    }
}


//
//  SubmissionView.swift
//  coRun
//
//  Created by Marselus Richard on 14/09/22.
//

import SwiftUI

struct SubmissionView: View {
    @StateObject var trainingViewModel : TrainingViewModel
    @StateObject var trainingSession : TrainingSession
    
    @StateObject var viewModel = SubmissionViewModel()
    @State var selectedIndex = -1
    
    var body: some View {
        VStack(spacing:20){
            Text("Activities This Week")
                .modifier(TextModifier.Title_2())
                .modifier(FillToLeftFrame())
            
            if(viewModel.displayRunningActivity.count==0){
                VStack{
                    Text("Activity Not Found")
                        .modifier(TextModifier.Headline())
                        .multilineTextAlignment(.center)
                        .modifier(ColorModifier.PrimaryGray())
                        .padding(.bottom,10)
                    
                    Text("You don't have any activity for the last 7 days.")
                    .modifier(TextModifier.Body())
                    .multilineTextAlignment(.center)
                    .modifier(ColorModifier.LightGray())
                    .padding(.bottom,10)

                }
                .modifier(ViewStyleModifier.CardBackgroundModifier())
            }else{
                ForEach(viewModel.displayRunningActivity.indices){ index in
                    SubmissionItemCard(runningData: viewModel.displayRunningActivity[index],isSelected: selectedIndex == index ? true : false)
                        .onTapGesture {
                            selectedIndex=index
                        }
                }
            }
            
            Spacer()
            
            if(selectedIndex == -1){
                Button("Select"){
                }
                .buttonStyle(ButtonStyleModifier.DisabledFillFrameButton())
            }
            else{
                Button("Select"){
                    viewModel.showSubmissionConfirmationAlert = true
                }
                .buttonStyle(ButtonStyleModifier.EnabledFillFrameButton())
                .alert(isPresented:$viewModel.showSubmissionConfirmationAlert){
                    Alert(
                        title: Text("Add This Data?"),
                        message: Text("This data will be submitted to your activity"),
                        primaryButton: .default(Text("Yes")){
                        //CODE HERE
//                            trainingSession.activityId=viewModel.displayRunningActivity[selectedIndex].uuid ?? ""
                            trainingSession.status=1
                            trainingViewModel.appendNewUserActivity(newData: viewModel.displayRunningActivity[selectedIndex])
                            
                            //Close View
                            viewModel.showSubmissionConfirmationAlert=false
                            trainingViewModel.showingSubmissionView=false
                        },
                        secondaryButton: .cancel()
                    )
                }
            }
        }
        .padding(20)
        .task{
            do{
                await viewModel.getRunningActivityFromHealthKit()
                viewModel.refreshDisplayData()
            }
        }
    }
}

//struct SubmissionView_Previews: PreviewProvider {
//    static var previews: some View {
//        SubmissionView()
//    }
//}

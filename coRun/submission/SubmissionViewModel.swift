//
//  SubmissionViewModel.swift
//  coRun
//
//  Created by Marselus Richard on 14/09/22.
//

import SwiftUI
import HealthKit

class SubmissionViewModel:ObservableObject, Identifiable{
    @Published var showSubmissionConfirmationAlert=false
    var loadRunningActivity = [Running]()
    @Published var displayRunningActivity = [Running]()
    
    func getRunningActivityFromHealthKit() async{
        let healthKit = HealthKitUtils()
        var hkWorkoutList = [HKWorkout]()
        loadRunningActivity = [Running]()
        
        let durationFormatter = DateComponentsFormatter()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        await hkWorkoutList = healthKit.readWorkouts() ?? [HKWorkout]()

        //print(hkWorkoutList.count)
        
        hkWorkoutList.forEach{data in
            let totalDistance = (data.totalDistance!.doubleValue(for: HKUnit.meter()) ) / 1000
            let totalDuration = data.duration
            
            loadRunningActivity.append(
                Running(
                    uuid: data.uuid.uuidString,
                    date: dateFormatter.string(from: data.startDate),
                    distance: Float(totalDistance),
                    duration: durationFormatter.string(from: totalDuration) ?? "",
                    pace: durationFormatter.string(from: totalDuration / totalDistance) ?? "",
                    indoor: (data.metadata?["HKIndoorWorkout"] as! Int) == 0 ? false : true))
        }
    }
    func refreshDisplayData(){
        displayRunningActivity=loadRunningActivity
    }
}

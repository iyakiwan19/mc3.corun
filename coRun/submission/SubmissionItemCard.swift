//
//  SubmissionItemCard.swift
//  coRun
//
//  Created by Marselus Richard on 14/09/22.
//

import SwiftUI

struct SubmissionItemCard: View {
    var runningData:Running
    var isSelected:Bool
    
    var body: some View {
        if(isSelected){
            VStack(alignment:.leading, spacing: 10){
                HStack{
                    Text(getDayNameFromDate(dateInYYYYMMDD:runningData.date ?? "2000-01-01"))
                        .modifier(TextModifier.Headline())
                    Text(" - ")
                    Text(reformatYYYYMMDDtoDDMMYYYYStringFormat(dateInYYYYMMDD:runningData.date ?? "2000-01-01"))
                }
                Text(String(runningData.distance ?? 0)+" Km")
                    .modifier(TextModifier.Footnote())
            }
            .modifier(FillToLeftFrame())
            .modifier(ViewStyleModifier.StrokeBackgroundModifier())
        }
        else{
            VStack(alignment:.leading, spacing:10){
                HStack{
                    Text(getDayNameFromDate(dateInYYYYMMDD:runningData.date ?? "2000-01-01"))
                        .modifier(TextModifier.Headline())
                    Text(" - ")
                    Text(reformatYYYYMMDDtoDDMMYYYYStringFormat(dateInYYYYMMDD:runningData.date ?? "2000-01-01"))
                }
                Text(String(runningData.distance ?? 0)+" Km")
                    .modifier(TextModifier.Footnote())
            }
            .modifier(FillToLeftFrame())
            .modifier(ViewStyleModifier.CardBackgroundModifier())
            .onAppear(){
            print(runningData.date ?? "")
        }
    }
    }
}

//struct SubmissionItemCard_Previews: PreviewProvider {
//    static var previews: some View {
//        SubmissionItemCard()
//    }
//}

//
//  ProfileModel.swift
//  coRun
//
//  Created by Taufiq Ichwanusofa on 14/09/22.
//

import Foundation

struct ProfileModel: Decodable {
    var id: Int?
    var first_name: String?
    var last_name: String?
    var sex: Int?
    var birth_date: String?
    var height: Int?
    var weight: Int?
    var uuid: String?
    var photo: String?
    var status: String?
    var createdAt: String?
    var updatedAt: String?
    
    init() {}
    
    init(id: Int, first_name: String, last_name: String, sex: Int, birth_date: String, height: Int, weight: Int, uuid: String, photo: String, status: String, createdAt: String, updatedAt: String) {
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.sex = sex
        self.birth_date = birth_date
        self.height = height
        self.weight = weight
        self.uuid = uuid
        self.photo = photo
        self.status = status
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

//
//  ProfileView.swift
//  coRun
//
//  Created by Taufiq Ichwanusofa on 12/09/22.
//

import SwiftUI

struct ProfileView: View {
    @ObservedObject var viewModelProfile = ProfileViewModel()
    @AppStorage("uuid") var uuid = ""
    @State var isShowLogin = false
    
    var body: some View {
        if uuid.isEmpty {
            ScrollView {
                VStack(spacing: 10) {
                    HStack {
                        Spacer()
                        Image(systemName: "person.circle.fill")
                            .font(.system(size: 100.0))
                            .foregroundColor(ColorPalette().primaryGreen)
                        Spacer()
                        VStack(alignment: .leading) {
                            Text("Please login")
                                .modifier(TextModifier.Title_1())
                                .modifier(FillToLeftFrame())
                            Button(action: {
                                print("go to trainee page")
                            }) {
                                HStack {
                                    Text("Your Trainee")
                                        .foregroundColor(.white)
                                        .modifier(TextModifier.BodyBold())
                                        .modifier(FillToLeftFrame())
                                    Spacer()
                                    Image(systemName: "chevron.right")
                                        .foregroundColor(.white)
                                        .modifier(TextModifier.BodyBold())
                                }
                                .padding(10.0)
                            }
                            .background(ColorPalette().primaryGreen)
                            .frame(width: 200.0)
                            .cornerRadius(10.0)
                            .disabled(true)
                        }
                        Spacer()
                    }
                }
            }
            .onTapGesture {
                isShowLogin = true
            }
            .fullScreenCover(isPresented: $isShowLogin) {
                LoginView()
            }
        } else {
            ScrollView {
                VStack(spacing: 10) {
                    HStack {
                        Spacer()
                        Image(systemName: "person.circle.fill")
                            .font(.system(size: 100.0))
                            .foregroundColor(ColorPalette().primaryGreen)
                        Spacer()
                        VStack(alignment: .leading) {
                            Text("\(viewModelProfile.dataProfile?.first_name ?? "") \(viewModelProfile.dataProfile?.last_name ?? "")")
                                .modifier(TextModifier.Title_1())
                                .modifier(FillToLeftFrame())
                            NavigationLink {
                                ListTraineeView()
                            } label: {
                                HStack {
                                    Text("Your Trainee")
                                        .foregroundColor(.white)
                                        .modifier(TextModifier.BodyBold())
                                        .modifier(FillToLeftFrame())
                                    Spacer()
                                    Image(systemName: "chevron.right")
                                        .foregroundColor(.white)
                                        .modifier(TextModifier.BodyBold())
                                }
                                .padding(10.0)
                                .background(ColorPalette().primaryGreen)
                                .frame(width: 200.0)
                                .cornerRadius(10.0)

//                                Button(action: {
//                                    print("go to trainee page")
//                                }) {
//                                    HStack {
//                                        Text("Your Trainee")
//                                            .foregroundColor(.white)
//                                            .modifier(TextModifier.BodyBold())
//                                            .modifier(FillToLeftFrame())
//                                        Spacer()
//                                        Image(systemName: "chevron.right")
//                                            .foregroundColor(.white)
//                                            .modifier(TextModifier.BodyBold())
//                                    }
//                                    .padding(10.0)
//                                }
//                                .background(ColorPalette().primaryGreen)
//                                .frame(width: 200.0)
//                                .cornerRadius(10.0)
                            }

                            
                            
                        }
                        Spacer()
                    }
                    SummaryView(viewModelProfile: viewModelProfile)
                }
            }
            .onAppear(perform: viewModelProfile.getRunningProfile)
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}

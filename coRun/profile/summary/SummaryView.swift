//
//  SummaryView.swift
//  coRun
//
//  Created by Taufiq Ichwanusofa on 12/09/22.
//

import SwiftUI
import SwiftUICharts

struct SummaryView: View {
    let data: [SummaryModel] = [
        SummaryModel(week: 0, distance: 19.5),
        SummaryModel(week: 1, distance: 23.0),
        SummaryModel(week: 2, distance: 23.3),
        SummaryModel(week: 3, distance: 24.15),
        SummaryModel(week: 4, distance: 20.0),
        SummaryModel(week: 5, distance: 25.0),
        SummaryModel(week: 6, distance: 32.0)
    ]
    
    let points: LineChartData = weekOfData()
    var viewModelProfile: ProfileViewModel?
    
    init(viewModelProfile: ProfileViewModel) {
        self.viewModelProfile = viewModelProfile
    }
    
    var body: some View {
        VStack(spacing: 10.0) {
            Text("Marathon Training Plan")
                .modifier(TextModifier.BodySemiBold())
                .modifier(FillToLeftFrame())
            Text("This Week")
                .modifier(TextModifier.BodySemiBold())
                .modifier(FillToLeftFrame())
            HStack(alignment: .lastTextBaseline) {
                Text(viewModelProfile?.calculateTotalDistance() ?? "0.0")
                    .modifier(TextModifier.LargeTitleSemiBold())
                    .foregroundColor(ColorPalette().primaryGreen)
                Text("Kilometers")
                    .modifier(TextModifier.Caption_1())
            }
            .modifier(FillToLeftFrame())
            
            ScrollView(.horizontal, showsIndicators: false) {
                LineChart(chartData: points)
                    .pointMarkers(chartData: points)
                    .xAxisLabels(chartData: points)
                    .yAxisLabels(chartData: points)
                    .touchOverlay(chartData: points)
                    .infoBox(chartData: points)
                    .frame(
                        minWidth: 100,
                        idealWidth: 500,
                        maxWidth: 900,
                        minHeight: 300,
                        idealHeight: 300,
                        maxHeight: 300,
                        alignment: .center
                    )
            }
            
            HStack {
                VStack(alignment: .center) {
                    Text("6'09''")
                        .modifier(TextModifier.CalloutSemiBold())
                    Text("Avg. Pace")
                        .modifier(TextModifier.Caption_1())
                }
                Spacer()
                VStack(alignment: .center) {
                    Text("3:50:26")
                        .modifier(TextModifier.CalloutSemiBold())
                    Text("Time")
                        .modifier(TextModifier.Caption_1())
                }
                Spacer()
                VStack(alignment: .center) {
                    Text("4")
                        .modifier(TextModifier.CalloutSemiBold())
                    Text("Activities")
                        .modifier(TextModifier.Caption_1())
                }
            }
        }
        .padding()
        .background(ColorPalette().baseWhite)
    }
}

struct SummaryView_Previews: PreviewProvider {
    static var previews: some View {
        SummaryView(viewModelProfile: ProfileViewModel())
    }
}

func weekOfData() -> LineChartData {
        let data = LineDataSet(
            dataPoints: [
                LineChartDataPoint(value: 20, xAxisLabel: "W1", description: "Week 1"),
                LineChartDataPoint(value: 30, xAxisLabel: "W2", description: "Week 2"),
                LineChartDataPoint(value: 25,  xAxisLabel: "W3", description: "Week 3"),
                LineChartDataPoint(value: 10, xAxisLabel: "W4", description: "Week 4"),
                LineChartDataPoint(value: 15, xAxisLabel: "W5", description: "Week 5"),
                LineChartDataPoint(value: 22, xAxisLabel: "W6", description: "Week 6"),
                LineChartDataPoint(value: 40,  xAxisLabel: "W7", description: "Week 7")
            ],
            legendTitle: "Kilometres",
            pointStyle: PointStyle(pointSize: 10, borderColour: ColorPalette().primaryGreen, fillColour: .white, lineWidth: 2, pointType: .filledOutLine, pointShape: .circle),
            style: LineStyle(
                lineColour: ColourStyle(colour: ColorPalette().primaryGreen),
                lineType: .curvedLine
            )
        )
        
        let metadata   = ChartMetadata(title: "Step Count", subtitle: "Over a Week")
        
        let gridStyle  = GridStyle(
            numberOfLines: 7,
            lineColour   : Color(.lightGray).opacity(0.5),
            lineWidth    : 1,
            dash         : [6],
            dashPhase    : 0
        )
        
        let chartStyle = LineChartStyle(
            infoBoxPlacement    : .infoBox(isStatic: false),
            infoBoxBorderColour : Color.primary,
            infoBoxBorderStyle  : StrokeStyle(lineWidth: 1),
            xAxisGridStyle      : gridStyle,
            xAxisLabelPosition  : .bottom,
            xAxisLabelColour    : Color.primary,
            xAxisLabelsFrom     : .dataPoint(rotation: .degrees(0)),
            
            yAxisGridStyle      : gridStyle,
            yAxisLabelPosition  : .leading,
            yAxisLabelColour    : Color.primary,
            yAxisNumberOfLabels : 5,
            
            baseline            : .minimumWithMaximum(of: 0),
            topLine             : .maximum(of: 50),
            
            globalAnimation     : .easeOut(duration: 1)
        )
        
        return LineChartData(
            dataSets       : data,
            metadata       : metadata,
            chartStyle     : chartStyle
        )
    }

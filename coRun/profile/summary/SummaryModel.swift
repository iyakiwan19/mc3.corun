//
//  SummaryModel.swift
//  coRun
//
//  Created by Taufiq Ichwanusofa on 13/09/22.
//

import Foundation

struct SummaryModel {
    let week: Int?
    let distance: Float?
        
    init(week: Int, distance: Float) {
        self.week = week
        self.distance = distance
    }
}

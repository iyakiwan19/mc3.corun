//
//  ProfileViewModel.swift
//  coRun
//
//  Created by Taufiq Ichwanusofa on 14/09/22.
//

import Foundation
import SwiftyJSON
import SwiftUI

class ProfileViewModel: ObservableObject {
//    @AppStorage("coruntoken") var coruntoken = ""
    @Published var dataProfile: ProfileModel?
    @Published var dataRunningActivities = [RunningActivityModel]()
    let coruntoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTMsInV1aWQiOiJ0YXVmaXExMjMiLCJpYXQiOjE2NjMyMDUzMzl9.Nvce0ZIAJgptC1hBAEwiH8WjBrZyaSCXWKCo0RTnjnY"
    
    func getRunningProfile() {
        self.getMyProfile()
        self.getMyRunningActivities()
    }
    
    private func getMyProfile() {
        let urlString = "https://mcedevs.com/user/get-my-profile"
        
        guard let url = URL(string: urlString) else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(coruntoken, forHTTPHeaderField: "coruntoken")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, res, error in
            do {
                if let data = data {
                    let result = try JSONDecoder().decode(ProfileModel.self, from: data)
                    print(result)
                    DispatchQueue.main.async {
                        self.dataProfile = result
                    }
                }
                
            } catch (let error) {
                print(error.localizedDescription)
            }
        }
        .resume()
    }
    
    private func getMyRunningActivities() {
        let urlString = "https://mcedevs.com/running-session/my-running-activities"
        
        guard let url = URL(string: urlString) else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(coruntoken, forHTTPHeaderField: "coruntoken")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { data, res, error in
            do {
                if let data = data {
                    let result = try JSONDecoder().decode([RunningActivityModel].self, from: data)
                    print(result)
                    DispatchQueue.main.async {
                        self.dataRunningActivities = result
                    }
                }
                
            } catch (let error) {
                print(error.localizedDescription)
            }
        }
        .resume()
    }
    
    func calculateTotalDistance() -> String {
        var totalDistance = 0.0
        for dataRunning in dataRunningActivities {
            totalDistance += dataRunning.distance ?? 0.0
        }
        
        return "\(totalDistance)"
    }
}

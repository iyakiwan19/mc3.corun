//
//  RunningActivityModel.swift
//  coRun
//
//  Created by Taufiq Ichwanusofa on 15/09/22.
//

import Foundation

struct RunningActivityModel: Decodable {
    var id: Int?
    var date: String?
    var duration: String?
    var pace: String?
    var uuid_running: String?
    var route: String?
    var distance: Double?
    var status: String?
    var TrainingPlanId: Int?
    var UserId: Int?
    var createdAt: String?
    var updatedAt: String?
    var TrainingPlan: TrainingPlanActivityModel?
    
    init() { }
    
    init(id: Int, date: String, duration: String, pace: String, uuid_running: String, route: String, distance: Double, status: String, TrainingPlanId: Int, UserId: Int, createdAt: String, updatedAt: String, TrainingPlan: TrainingPlanActivityModel) {
        self.id = id
        self.date = date
        self.duration = duration
        self.pace = pace
        self.uuid_running = uuid_running
        self.route = route
        self.distance = distance
        self.status = status
        self.TrainingPlanId = TrainingPlanId
        self.UserId = UserId
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.TrainingPlan = TrainingPlan
    }
}

struct TrainingPlanActivityModel: Decodable {
    var id: Int?
    var week: Int?
    var day: Int?
    var date: String?
    var training_type: String?
    var distance: Double?
    var duration: String?
    var pace: String?
    var description: String?
    var status: String?
    var AssessmentId: Int?
    var createdAt: String?
    var updatedAt: String?
    
    init() { }
    
    init(id: Int, week: Int, day: Int, date: String, training_type: String, distance: Double, duration: String, pace: String, description: String, status: String, AssessmentId: Int, createdAt: String, updatedAt: String) {
        self.id = id
        self.week = week
        self.day = day
        self.date = date
        self.training_type = training_type
        self.distance = distance
        self.duration = duration
        self.pace = pace
        self.description = description
        self.status = status
        self.AssessmentId = AssessmentId
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

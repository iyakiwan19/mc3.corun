import Foundation

class AppNavigationViewModel: ObservableObject {
//    let tabBarImageNames = ["list.bullet.rectangle.portrait.fill", "figure.walk", "person.fill"]
//    let tabBarTextNames = ["Plan", "Run","Profile"]
//    let navBarTitle=["Training Plan","Run","Profile"]
    
    let tabBarImageNames = ["list.bullet.rectangle.portrait.fill","person.fill"]
    let tabBarTextNames = ["Plan","Profile"]
    let navBarTitle=["Training Plan","Profile"]
    
    var selectedTabIndex=0
    //var navBarTitle="Run"
    
    func updateSelectedTabIndex(newIndex:Int){
        selectedTabIndex=newIndex
    }
//    func updateNavBarTile(newTitle:String){
//        navBarTitle=newTitle
//    }
    
}

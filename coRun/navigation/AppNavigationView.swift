//
//  NavigationView.swift
//  coRun
//
//  Created by Marselus Richard on 30/08/22.
//

import SwiftUI
import HealthKit

struct AppNavigationView: View {
    @State var isReady = false
    @StateObject var viewModel = AppNavigationViewModel()
    
    var body: some View {
        TabView(selection: $viewModel.selectedTabIndex){
            ForEach(0..<2){ index in
            NavigationView{
                ScrollView{
                    switch index{
                    //INSERT VIEW HERE
                    case 0:
                        //Training View
                        TrainingView()
                    case 1:
                        //Profile View
                        ProfileView()
                    default:
                        ContentView()
                    }
                }
                .navigationTitle(viewModel.navBarTitle[index])
            }
            .navigationViewStyle(.stack)
            .tabItem {
                VStack{
                    Image(systemName: viewModel.tabBarImageNames[index])
                        .renderingMode(.template)
                    Text(viewModel.tabBarTextNames[index])
                }
            }
            .tag(index)
        }
    }
    .accentColor(ColorPalette.init().primaryGreen)
    .task {
        if !HKHealthStore.isHealthDataAvailable() {
            return
        }
        
        guard await HealthKitUtils().requestPermission() == true else {
            return
        }
        
        isReady = true
    }
}
}

struct NavigationView_Previews: PreviewProvider {
    static var previews: some View {
        AppNavigationView()
    }
}

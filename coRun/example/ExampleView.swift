//
//  ExampleView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 26/08/22.
//

import SwiftUI

struct FirstView: View {
    var body: some View {
        NavigationView {
            Text("First view")
                .navigationBarTitle(Text("First view"), displayMode: .inline)
                .navigationBarItems(trailing:
                                        NavigationLink("To second", destination: SecondView())
                )
        }
    }
}
struct SecondView: View {
    @State var isNavigationLinkActive = false
    var body: some View {
        VStack {
            Text("Second view")
            NavigationLink("To third", destination: ThirdView(), isActive: $isNavigationLinkActive)
                .hidden()
        }
        .navigationBarTitle(Text("Second view"), displayMode: .inline)
        .navigationBarItems(trailing:
                                Button("To third", action: {
            isNavigationLinkActive = true
        })
        )
    }
}
struct ThirdView: View {
    @State var isNavigationLinkActive = false
    var body: some View {
        VStack {
            Text("Third view")
            NavigationLink("To fourth", destination: FourthView(), isActive: $isNavigationLinkActive)
                .hidden()
        }
        .navigationBarTitle(Text("Third view"), displayMode: .inline)
        .navigationBarItems(trailing:
                                Button("To fourth", action: {
            isNavigationLinkActive = true
        })
        )
    }
}
struct FourthView: View {
    var body: some View {
        Text("Fourth view")
            .navigationBarTitle(Text("Fourth view"), displayMode: .inline)
    }
}

struct ContentView12: View {
    @State private var selectedStrength = "Mild"
    let strengths = ["Mild", "Medium", "Mature"]

    var body: some View {
        NavigationView {
            Form {
                Section {
                    Picker("Strength", selection: $selectedStrength) {
                        ForEach(strengths, id: \.self) {
                            Text($0)
                        }
                    }
                    .pickerStyle(.wheel)
                }
            }
            .navigationTitle("Select your cheese")
        }
    }
}

struct ExampleView: View {
    var body: some View {
        List {
            Text("Integer")
            Text("String")
            Text("Float")
        }
    }
}

struct ExampleView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView12()
    }
}

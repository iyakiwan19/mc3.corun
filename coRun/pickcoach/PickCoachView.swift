//
//  PickCoachView.swift
//  coRun
//
//  Created by Ricky Suprayudi on 06/09/22.
//

import SwiftUI

struct PickCoachView: View {
    
    @Environment(\.presentationMode) var presentationMode
    var coachUUID = ""
    var coachName = "Tono Tonoman"
    
    @AppStorage("coachID") var coachID = ""
    
    
    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    Spacer()
                    Text("\(coachName)")
                        .font(.system(size: 34, weight: .semibold))
                    Spacer()
                }
                .padding(.top, 50)
                HStack {
                    Spacer()
                    Image(systemName: "checkmark.circle.fill")
                        .font(.system(size: 180))
                        .modifier(ColorModifier.PrimaryGreen())
                    Spacer()
                }
                .padding(.vertical, 25)
                HStack {
                    Spacer()
                    Text("Will be assigned")
                        .font(.system(size: 24))
                    Spacer()
                }
                HStack {
                    Spacer()
                    Text("\nas your personal coach.\n\nYour training plan and feedback\n will be given by this coach.")
                        .font(.system(size: 17))
                        .multilineTextAlignment(.center)
                    Spacer()
                }
                Spacer()
                HStack {
                    Button {
                        coachID = coachUUID
                        print("coach confirmed = \(coachID)")
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        Text("Confirm")
                    }
                    .buttonStyle(ButtonStyleModifier.PrimaryGreenButton())
                }
                .padding(.horizontal, 25)
            }
            .toolbar(content: {
                ToolbarItem(placement: .primaryAction) {
                    Button {
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        Image(systemName: "xmark")
                            .modifier(TextModifier.Headline())
                            .foregroundColor(.red)
                    }
                }
                ToolbarItem(placement: .principal) {
                    Text("Coach Profile")
                        .modifier(TextModifier.Headline())
                }
            })
        }
    }
}

struct PickCoachView_Previews: PreviewProvider {
    static var previews: some View {
        PickCoachView()
    }
}


//
//  ColorModifier.swift
//  coRun
//
//  Created by Marselus Richard on 29/08/22.
//

// Based on Design Agreement 29/08/22

import SwiftUI

class ColorModifier{
    struct PrimaryGreen:ViewModifier{
        func body(content:Content) -> some View{
            content
                .foregroundColor(ColorPalette.init().primaryGreen)
        }
    }
    struct LightGreen:ViewModifier{
        func body(content:Content) -> some View{
            content
                .foregroundColor(ColorPalette.init().lightGreen)
        }
    }
    struct PrimaryGray:ViewModifier{
        func body(content:Content) -> some View{
            content
                .foregroundColor(ColorPalette.init().primaryGray)
        }
    }
    struct LightGray:ViewModifier{
        func body(content:Content) -> some View{
            content
                .foregroundColor(ColorPalette.init().lightGray)
        }
    }
    struct BaseWhite:ViewModifier{
        func body(content:Content) -> some View{
            content
                .foregroundColor(ColorPalette.init().baseWhite)
        }
    }
    struct textColor:ViewModifier{
        func body(content:Content) -> some View{
            content
                .foregroundColor(ColorPalette.init().textColor)
        }
    }
}

struct ColorPalette{
    let primaryGreen = Color("primaryGreen")
    let lightGreen = Color("lightGreen")
    let primaryGray = Color("primaryGray")
    let lightGray = Color("lightGray")
    let textColor = Color("textColor")
    //let backgroundGray =

    let baseWhite = Color("baseBackground")
    let buttonEnabled = Color("buttonEnabled")
    let buttonDisabled = Color("buttonDisabled")
}

//struct ColorPalette{
//    let primaryGreen = Color(red: 52/255, green: 199/255, blue: 89/255)
//    let lightGreen = Color(red: 48/255, green: 209/255, blue: 88/255)
//    let primaryGray = Color(red: 99/255, green: 99/255, blue: 102/255)
//    let lightGray = Color(red: 174/255, green: 174/255, blue: 178/255)
//
//    let baseWhite = Color(red: 249/255, green: 249/255, blue: 249/255)
//}

//
//  HealthKitUtils.swift
//  coRun
//
//  Created by Taufiq Ichwanusofa on 03/09/22.
//

import Foundation
import HealthKit

class HealthKitUtils {
    let store = HKHealthStore()

    func requestPermission() async -> Bool {
        let write: Set<HKSampleType> = []
        let read: Set = [
            HKQuantityType.init(HKQuantityTypeIdentifier.distanceWalkingRunning),
            HKObjectType.workoutType()
        ]
        
        let res: ()? = try? await store.requestAuthorization(toShare: write, read: read)
        guard res != nil else {
            return false
        }
        
        return true
    }
    
    func convertRunningData(_ workouts: [HKWorkout]) -> [Running] {
        var outputWorkouts = [Running]()
        let durationFormatter = DateComponentsFormatter()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        workouts.forEach {
            let totalDistance = ($0.totalDistance!.doubleValue(for: HKUnit.meter()) ) / 1000
            let totalDuration = $0.duration 
            outputWorkouts.append(Running(uuid: $0.uuid.uuidString, date: dateFormatter.string(from: $0.startDate), distance: Float(totalDistance), duration: durationFormatter.string(from: totalDuration) ?? "", pace: durationFormatter.string(from: totalDuration / totalDistance) ?? "", indoor: ($0.metadata?["HKIndoorWorkout"] as! Int) == 0 ? false : true))
        }
        
        return outputWorkouts
    }
    
    func readWorkouts() async -> [HKWorkout]? {
        let startDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        
        let running = HKQuery.predicateForWorkouts(with: .running)
        let predicateDate = HKQuery.predicateForSamples(withStart: startDate, end: Date(), options: HKQueryOptions())
        let compound = NSCompoundPredicate(andPredicateWithSubpredicates: [
            running,
            predicateDate
        ])
        
        let samples = try! await withCheckedThrowingContinuation { (continuation: CheckedContinuation<[HKSample], Error>) in
            store.execute(HKSampleQuery(sampleType: .workoutType(), predicate: compound, limit: HKObjectQueryNoLimit, sortDescriptors: [.init(keyPath: \HKSample.startDate, ascending: false)], resultsHandler: { query, samples, error in
                    if let hasError = error {
                        continuation.resume(throwing: hasError)
                        return
                    }
                
                    guard let samples = samples else {
                        fatalError("*** Invalid State: This can only fail if there was an error. ***")
                    }
                    
                    continuation.resume(returning: samples)
                }
            ))
        }
        
        guard let workouts = samples as? [HKWorkout] else {
            return nil
        }
        
        return workouts
    }
}

struct Running{
    let uuid: String?
    let date: String?
    let distance: Float?
    let duration: String?
    let pace: String?
    let indoor: Bool?
    
    init(uuid: String, date: String, distance: Float, duration: String, pace: String, indoor: Bool) {
        self.uuid = uuid
        self.date = date
        self.distance = distance
        self.duration = duration
        self.pace = pace
        self.indoor = indoor
    }
}

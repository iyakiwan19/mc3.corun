//
//  TextModifier.swift
//  coRun
//
//  Created by Marselus Richard on 29/08/22.
//

// Based on Design Agreement 29/08/22

import SwiftUI

//FONT TYPE
class TextModifier{
    struct LargeTitle:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:34,weight: .bold))
        }
    }
    struct LargeTitleSemiBold:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:34,weight: .semibold))
        }
    }
    struct Title_1:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:28,weight: .semibold))
        }
    }
    struct Title_2:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:22,weight: .semibold))
        }
    }
    struct Title_3:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:20,weight: .semibold))
        }
    }
    struct Headline:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:17,weight: .semibold))
        }
    }
    struct Body:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:17,weight: .regular))
        }
    }
    struct BodySemiBold:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:17,weight: .semibold))
        }
    }
    struct BodyBold:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:17,weight: .bold))
        }
    }
    struct Callout:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:16,weight: .regular))
        }
    }
    struct CalloutSemiBold:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:16,weight: .semibold))
        }
    }
    struct SubHead:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:15,weight: .regular))
        }
    }
    struct Footnote:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:13,weight: .regular))
        }
    }
    struct Caption_1:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:12,weight: .regular))
        }
    }
    struct Caption_2:ViewModifier{
        func body(content:Content) -> some View{
            content
                .font(.system(size:11,weight: .regular))
        }
    }
    
    
//    struct AlignmentLeading:ViewModifier{
//        func body(content:Content) -> some View{
//            content
//                .frame(alignment: .leading)
//        }
//    }
//    struct AlignmentTrailing:ViewModifier{
//        func body(content:Content) -> some View{
//            content
//                .frame(alignment: .trailing)
//        }
//    }
//    struct AlignmentCenter:ViewModifier{
//        func body(content:Content) -> some View{
//            content
//                .frame(alignment: .center)
//        }
//    }
//    struct Italic:ViewModifier{
//        func body(content:Content) -> some View{
//            content
//                .font(Font.italic())
//        }
//    }
}

//BORDER STYLE
struct FillFrame:ViewModifier{
    func body(content: Content) -> some View {
        content
            .frame(minWidth:0,maxWidth: .infinity)
    }
}
struct FillToLeftFrame:ViewModifier{
    func body(content: Content) -> some View {
        content
            .frame(minWidth:0,maxWidth: .infinity,alignment: .leading)
    }
}

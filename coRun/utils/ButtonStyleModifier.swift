//
//  ButtonStyleModifier.swift
//  coRun
//
//  Created by Marselus Richard on 29/08/22.
//

// Based on Design Agreement 29/08/22

import SwiftUI

class ButtonStyleModifier{
    struct EnabledFillFrameButton: ButtonStyle {
        func makeBody(configuration: Configuration) -> some View {
            configuration.label
                .padding()
                .frame(minWidth:0,maxWidth: .infinity)
                .background(configuration.isPressed ? ColorPalette.init().primaryGreen : ColorPalette.init().lightGreen)
                .foregroundColor(.white)
                .cornerRadius(10)
                .scaleEffect(configuration.isPressed ? 0.95 : 1.0)
        }
    }
    
    struct PrimaryGreenButton: ButtonStyle {
        func makeBody(configuration: Configuration) -> some View {
            configuration.label
                .padding()
                .frame(minWidth:0,maxWidth: .infinity)
                .background(configuration.isPressed ? ColorPalette.init().primaryGreen : ColorPalette.init().lightGreen)
                .foregroundColor(.white)
                .cornerRadius(10)
                .scaleEffect(configuration.isPressed ? 0.95 : 1.0)
        }
    }
    struct SubmitCoachLinkButton: ButtonStyle {
        var disabled: Bool
        func makeBody(configuration: Configuration) -> some View {
            configuration.label
                .padding()
                .frame(minWidth:0,maxWidth: 62, minHeight: 0, maxHeight: 62)
                .background(disabled ? ColorPalette.init().buttonDisabled : (configuration.isPressed ? ColorPalette.init().primaryGreen : ColorPalette.init().lightGreen))
                .foregroundColor(.white)
                .cornerRadius(10)
                .scaleEffect(disabled ? 1.0 : (configuration.isPressed ? 0.95 : 1.0))
        }
    }
    struct DisabledFillFrameButton: ButtonStyle {
        func makeBody(configuration: Configuration) -> some View {
            configuration.label
                .padding()
                .frame(minWidth:0,maxWidth: .infinity)
                .background(ColorPalette.init().primaryGray)
                .foregroundColor(.white)
                .cornerRadius(10)
        }
    }
    struct HugFrameButton: ButtonStyle {
        var paddingButton:EdgeInsets
        
        func makeBody(configuration: Configuration) -> some View {
            configuration.label
                .padding(paddingButton)
                .background(configuration.isPressed ? ColorPalette.init().primaryGreen : ColorPalette.init().lightGreen)
                .foregroundColor(.white)
                .cornerRadius(10)
                .scaleEffect(configuration.isPressed ? 0.95 : 1.0)
        }
    }
}

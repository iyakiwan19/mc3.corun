//
//  WeeklyTrainingPlanCardView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 14/09/22.
//

import SwiftUI

struct WeeklyTrainingPlanCardView: View {
    let weaklyTrainingPlan : WeaklyTrainingPlan
    
    var body: some View {
        HStack{
            VStack(alignment:.leading, spacing:5){
                Text(weaklyTrainingPlan.week)
                    .font(.system(size: 15, weight: .bold))
                    .modifier(TextModifier.Body())
                Text(weaklyTrainingPlan.desc != "" ? weaklyTrainingPlan.desc : "Configure")
                    .font(.system(size: 12))
                    .foregroundColor(weaklyTrainingPlan.desc != "" ? .black : Color("primaryGreen"))
            }
            Spacer()
            Image(systemName: "chevron.right")
                .modifier(TextModifier.Body())
                .modifier(ColorModifier.PrimaryGray())
        }
        .padding(16)
        .background(
            RoundedRectangle(cornerRadius: 10, style: .continuous).fill( Color("textFieldBackground"))
        )
    }
}


struct WeeklyTrainingPlanCardView_Previews: PreviewProvider {
    static var previews: some View {
        WeeklyTrainingPlanCardView(weaklyTrainingPlan: dataWeaklyTrainingPlan[0])
    }
}

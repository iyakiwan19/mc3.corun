//
//  WeeklyTrainingPlanView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 14/09/22.
//

import SwiftUI

struct WeeklyTrainingPlanView: View {
    @State private var isDailyTrainingPlanView = false
    
    var body: some View {
        VStack {
            HStack{
                Text("5K Training")
                    .font(.system(size: 20, weight: .semibold))
                Spacer()
            }
            .padding(.vertical)
            ScrollView(showsIndicators: false) {
                ForEach(dataWeaklyTrainingPlan) { weaklyTrainingPlan in
                    WeeklyTrainingPlanCardView(weaklyTrainingPlan: weaklyTrainingPlan)
                        .onTapGesture {
                            isDailyTrainingPlanView = true
                        }
                }
                NavigationLink("Navigate To Daily Training Plan",destination: DailyTrainingPlanView(), isActive: $isDailyTrainingPlanView).hidden()
            }
        }
        .padding(.horizontal, 20)
    }
}

struct WeeklyTrainingPlanView_Previews: PreviewProvider {
    static var previews: some View {
        WeeklyTrainingPlanView()
    }
}

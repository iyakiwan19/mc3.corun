//
//  TraineeTrainingPlanModel.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 14/09/22.
//

import Foundation

struct WeaklyTrainingPlan: Identifiable {
    let id: Int
    let week: String
    let desc: String
}

var dataWeaklyTrainingPlan: [WeaklyTrainingPlan] = [
    .init(id: 0, week: "Week 1", desc: "Easy Run, Tempo Run, Long Run"),
    .init(id: 1, week: "Week 2", desc: "Easy Run, Interval Run, Long Run"),
    .init(id: 2, week: "Week 3", desc: "")
]

struct DailyTrainingPlan: Identifiable {
    let id: Int
    let day: String
    let desc: String
}

var dataDailyTrainingPlan: [DailyTrainingPlan] = [
    .init(id: 0, day: "Monday", desc: "Easy Run - 5km"),
    .init(id: 1, day: "Wednesday", desc: ""),
    .init(id: 2, day: "Friday", desc: "")
]

struct RunningTypeTrainingPlan: Identifiable {
    let id: Int
    let name: String
}

var dataRunningTypeTrainingPlanOdd: [RunningTypeTrainingPlan] = [
    .init(id: 0, name: "Easy/Recovery"),
    .init(id: 2, name: "Progressive"),
    .init(id: 4, name: "Fartlek"),
    .init(id: 6, name: "Interval"),
]

var dataRunningTypeTrainingPlanEven: [RunningTypeTrainingPlan] = [
    .init(id: 1, name: "Medium/Long"),
    .init(id: 3, name: "Continuous"),
    .init(id: 5, name: "Tempo"),
    .init(id: 7, name: "Other")
]

//
//  CostumeTrainingPlanView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 15/09/22.
//

import SwiftUI

struct CostumeTrainingPlanView: View {
    @StateObject var viewModel = CostumeTrainingPlanViewModel()
    @FocusState var keyboardFocus: Bool
    
    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack {
                HStack {
                    Image(systemName: "hare").font(.system(size: 20))
                    Text("Running Type")
                        .font(.system(size: 20, weight: .semibold))
                    Spacer()
                }
                HStack{
                    VStack{
                        ForEach(dataRunningTypeTrainingPlanOdd) { runningLevel in
                            CostumeTrainingPlanCardView(runningType: runningLevel.name, selected: viewModel.selectedRunningLevel == runningLevel.id)
                                .padding(EdgeInsets(top: 5, leading: 5, bottom: 0, trailing: 5))
                                .onTapGesture {
                                    viewModel.selectedRunningLevel = runningLevel.id
                                    //                            assesment.raceGoal = String(raceGoal.id)
                                }
                        }
                        
                    }
                    VStack{
                        ForEach(dataRunningTypeTrainingPlanEven) { runningLevel in
                            CostumeTrainingPlanCardView(runningType: runningLevel.name, selected: viewModel.selectedRunningLevel == runningLevel.id)
                                .padding(EdgeInsets(top: 5, leading: 5, bottom: 0, trailing: 5))
                                .onTapGesture {
                                    viewModel.selectedRunningLevel = runningLevel.id
                                    //                            assesment.raceGoal = String(raceGoal.id)
                                }
                        }
                        
                    }
                }
                Divider()
                    .padding(.vertical)
                HStack {
                    Image(systemName: "ruler").font(.system(size: 20))
                    Text("Running Distance (km)")
                        .font(.system(size: 20, weight: .semibold))
                    Spacer()
                }
                TextField("", text: $viewModel.distance)
                    .padding()
                    .font(.body)
                    .frame(height: 44)
                    .background(Color(UIColor.systemGray6))
                    .keyboardType(.decimalPad)
                    .cornerRadius(10)
                    .focused($keyboardFocus)
                    .disableAutocorrection(true)
                Divider()
                    .padding(.vertical)
                
                HStack {
                    Image(systemName: "square.and.pencil").font(.system(size: 20))
                    Text("Notes for Trainee")
                        .font(.system(size: 20, weight: .semibold))
                    Spacer()
                }
                TextField("", text: $viewModel.notes)
                    .padding()
                    .font(.body)
                    .frame(height: 44)
                    .background(Color(UIColor.systemGray6))
                    .cornerRadius(10)
                    .focused($keyboardFocus)
                    .disableAutocorrection(true)
                Spacer()
                
            }
            .padding(.horizontal, 20)
            .navigationBarTitle(Text("Week 3 - Monday"), displayMode: .inline)
            .toolbar {
                ToolbarItemGroup(placement: .keyboard) {
                    Spacer()
                    Button("Done") {
                        keyboardFocus = false
                    }
                }
            }
        }
    }
}

struct CostumeTrainingPlanView_Previews: PreviewProvider {
    static var previews: some View {
        CostumeTrainingPlanView()
    }
}

//
//  CostumeTrainingPlanViewModel.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 15/09/22.
//

import Foundation

class CostumeTrainingPlanViewModel: ObservableObject {
    @Published var selectedRunningLevel = -1
    @Published var distance = ""
    @Published var notes = ""
}

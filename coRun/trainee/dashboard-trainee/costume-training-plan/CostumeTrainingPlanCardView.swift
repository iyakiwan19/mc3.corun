//
//  CostumeTrainingPlanCardView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 15/09/22.
//

import SwiftUI

struct CostumeTrainingPlanCardView: View {
    let runningType: String
    let selected: Bool
    var body: some View {
        HStack{
            Text(runningType)
                .font(.system(size: 17, weight: selected ? .bold: .regular))
                .padding(.vertical, 12)
                .frame(maxWidth: .infinity)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color(UIColor.systemGray6), lineWidth: selected ? 1 : 0)
                )
        }
        .background(
            RoundedRectangle(cornerRadius: 10, style: .continuous).fill( Color("textFieldBackground"))
        )
    }
}

struct CostumeTrainingPlanCardView_Previews: PreviewProvider {
    static var previews: some View {
        CostumeTrainingPlanCardView(runningType: "5k Race", selected: true)
    }
}

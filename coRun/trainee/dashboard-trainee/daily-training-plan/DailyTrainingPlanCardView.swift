//
//  DailyTrainingPlanCardView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 15/09/22.
//

import SwiftUI

struct DailyTrainingPlanCardView: View {
    let dailyTrainingPlan : DailyTrainingPlan
    
    var body: some View {
        HStack{
            Text(dailyTrainingPlan.day)
                .font(.system(size: 17, weight: .bold))
                .modifier(TextModifier.Body())
            Text(dailyTrainingPlan.desc != "" ? dailyTrainingPlan.desc : "Configure")
                .font(.system(size: 15))
                .foregroundColor(dailyTrainingPlan.desc != "" ? .black : Color("primaryGreen"))
            Spacer()
            Image(systemName: "chevron.right")
                .modifier(TextModifier.Body())
                .modifier(ColorModifier.PrimaryGray())
        }
        .padding(16)
        .background(
            RoundedRectangle(cornerRadius: 10, style: .continuous).fill( Color("textFieldBackground"))
        )
    }
}

struct DailyTrainingPlanCardView_Previews: PreviewProvider {
    static var previews: some View {
        DailyTrainingPlanCardView(dailyTrainingPlan: dataDailyTrainingPlan[0])
    }
}

//
//  DailyTrainingPlanView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 14/09/22.
//

import SwiftUI

struct DailyTrainingPlanView: View {
    @State private var isCostumeTrainingPlanView = false
    
    var body: some View {
        VStack {
            HStack{
                Text("Week 3")
                    .font(.system(size: 20, weight: .semibold))
                Spacer()
            }
            .padding(.vertical)
            ScrollView(showsIndicators: false) {
                ForEach(dataDailyTrainingPlan) { dayTrainingPlan in
                    DailyTrainingPlanCardView(dailyTrainingPlan: dayTrainingPlan)
                        .onTapGesture {
                            isCostumeTrainingPlanView = true
                        }
                }
                NavigationLink("Navigate To Costume Training Plan",destination: CostumeTrainingPlanView(), isActive: $isCostumeTrainingPlanView).hidden()
            }
        }
        .padding(.horizontal, 20)
    }
}

struct DailyTrainingPlanView_Previews: PreviewProvider {
    static var previews: some View {
        DailyTrainingPlanView()
    }
}

//
//  DashboardTraineeView.swift
//  coRun
//
//  Created by Irfan Izudin on 12/09/22.
//

import SwiftUI

struct DashboardTraineeView: View {
    @State var emptyTrainingPlan = 0
    @State private var showingSheet = false
    @State private var isWeeklyTrainingPlanView = false
    @State private var isDailyTrainingPlanView = false
    @State private var isCreateTrainingPlanView = false
    
    var body: some View {
        VStack (alignment: .leading) {
            HStack {
                Image(systemName: "person.crop.circle.fill")
                    .font(.system(size: 68)).modifier(ColorModifier.PrimaryGreen())
                    .onTapGesture {
                        emptyTrainingPlan = 3
                    }
                VStack(alignment: .leading) {
                    Text("Irfan Izudin")
                        .font(.system(size: 28, weight: .semibold))
                    Text("5K Training")
                }
                Spacer()
                Image(systemName: "chevron.right")
                    .font(.system(size: 24))
                    .onTapGesture {
                        showingSheet = true
                    }
                    .sheet(isPresented: $showingSheet) {
                        ProfileTraineeView()
                    }
            }
            Divider()
            
            Text("Performance Overview")
                .font(.system(size: 20, weight: .semibold))
                .padding(.vertical)
            HStack {
                VStack(alignment: .center) {
                    Text("6’09’’")
                        .font(.system(size: 16, weight: .bold))
                    Text("Avg. Pace")
                        .font(.system(size: 12, weight: .regular))
                }
                Spacer()
                VStack(alignment: .center) {
                    Text("3:50:26")
                        .font(.system(size: 16, weight: .bold))
                    Text("Total Time")
                        .font(.system(size: 12, weight: .regular))
                }
                Spacer()
                VStack(alignment: .center) {
                    Text("284 KM")
                        .font(.system(size: 16, weight: .bold))
                    Text("Total Distance")
                        .font(.system(size: 12, weight: .regular))
                }
            }
            Divider()
            if emptyTrainingPlan > 0 {
                HStack{
                    Text("Training Plan")
                        .font(.system(size: 20, weight: .semibold))
                    Spacer()
                    Button {
                        isWeeklyTrainingPlanView = true
                    } label: {
                        Text("Show All Weeks")
                            .font(.system(size: 16))
                            .foregroundColor(Color("primaryGreen"))
                    }
                }
                .padding(.vertical)
                ScrollView(showsIndicators: false) {
                    ForEach(dataWeaklyTrainingPlan) { weaklyTrainingPlan in
                        WeeklyTrainingPlanCardView(weaklyTrainingPlan: weaklyTrainingPlan)
                            .onTapGesture {
                                isDailyTrainingPlanView = true
                            }
                    }
                    NavigationLink("Navigate To Daily Training Plan",destination: DailyTrainingPlanView(), isActive: $isDailyTrainingPlanView).hidden()
                }
                NavigationLink("Navigate To Weekly Training Plan",destination: WeeklyTrainingPlanView(), isActive: $isWeeklyTrainingPlanView).hidden()
            } else {
                Text("Training Plan")
                    .font(.system(size: 20, weight: .semibold))
                    .padding(.vertical)
                Button {
                    isCreateTrainingPlanView = true
                } label: {
                    Text("Create Training Plan")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .font(.system(size: 18))
                        .padding()
                        .foregroundColor(.white)
                        .background(
                            RoundedRectangle(cornerRadius: 10, style: .continuous).fill( Color("primaryGreen"))
                        )
                }
                NavigationLink("Navigate To Create Training Plan",destination: CreateTriningPlanView(), isActive: $isCreateTrainingPlanView).hidden()
            }
            Spacer()
        }
        .padding(.horizontal, 25)
    }
}

struct DashboardTraineeView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardTraineeView()
    }
}

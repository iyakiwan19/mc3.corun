//
//  CreateTriningPlanView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 15/09/22.
//

import SwiftUI

struct CreateTriningPlanView: View {
    @FocusState var keyboardFocus: Bool
    @State var weekDaily = ""
    
    var body: some View {
        VStack (spacing: 10){
            HStack {
                Text("Race Goal: ")
                    .font(.system(size: 20, weight: .semibold))
                Text("Half Marathon Race")
                    .font(.system(size: 20, weight: .regular))
                Spacer()
            }
            HStack {
                Text("Starting Level: ")
                    .font(.system(size: 20, weight: .semibold))
                Text("Intermediate")
                    .font(.system(size: 20, weight: .regular))
                Spacer()
            }
            
            VStack {
                HStack {
                    Text("24 Aug 2022 - Outdoor Run")
                        .font(.subheadline)
                    Spacer()
                }
                .padding(.vertical, 5)
                HStack {
                    VStack {
                        Text("8 km")
                            .font(.title)
                        Text("Distance")
                            .font(.caption)
                    }
                    Spacer()
                    VStack {
                        Text("8’00’’")
                            .font(.title)
                        Text("Avg. Pace")
                            .font(.caption)
                    }
                    Spacer()
                    VStack {
                        Text("1h 04m")
                            .font(.title)
                        Text("Duration")
                            .font(.caption)
                    }
                }
            }
            .padding()
            .overlay(RoundedRectangle(cornerRadius: 10)
                .stroke(Color(UIColor.systemGray), lineWidth: 1))
            .padding(.bottom)
            
            Divider()
            
            HStack {
                Text("Starting Week: ")
                    .font(.system(size: 20, weight: .semibold))
                Text("6 September 2022")
                    .font(.system(size: 20, weight: .regular))
                Spacer()
            }
            HStack {
                Image(systemName: "calendar").font(.system(size: 20))
                Text(" Program Duration (Weeks) ")
                    .font(.system(size: 20, weight: .semibold))
                Spacer()
            }
            TextField("", text: $weekDaily)
                .padding()
                .font(.body)
                .frame(height: 44)
                .background(Color(UIColor.systemGray6))
                .keyboardType(.decimalPad)
                .cornerRadius(10)
                .focused($keyboardFocus)
                .disableAutocorrection(true)
            Button {
                
            } label: {
                Text("Create")
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .font(.system(size: 18))
                    .padding()
                    .foregroundColor(.white)
                    .background(
                        RoundedRectangle(cornerRadius: 10, style: .continuous).fill( Color("primaryGreen"))
                    )
            }
            Spacer()
        }
        .padding(25)
        .navigationBarTitle(Text("Create Training Plan"), displayMode: .inline)
        .toolbar {
            ToolbarItemGroup(placement: .keyboard) {
                Spacer()
                Button("Done") {
                    keyboardFocus = false
                }
            }
        }
    }
}

struct CreateTriningPlanView_Previews: PreviewProvider {
    static var previews: some View {
        CreateTriningPlanView()
    }
}

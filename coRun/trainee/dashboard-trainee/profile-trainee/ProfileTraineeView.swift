//
//  ProfileTraineeView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 15/09/22.
//

import SwiftUI

struct ProfileTraineeView: View {
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        NavigationView{
            VStack{
                Image(systemName: "person.crop.circle.fill")
                    .font(.system(size: 100)).modifier(ColorModifier.PrimaryGreen())
                
                Text("Irfan Izudin")
                    .font(.system(size: 28, weight: .semibold))
                
                VStack {
                    VStack {
                        HStack {
                            Text("Age")
                            Spacer()
                            Text("23")
                        }
                        Divider()
                            .frame(height: 1)
                            .overlay(Color(UIColor.systemGray3))
                    }
                    .padding(.vertical, 10)
                    
                    VStack {
                        HStack {
                            Text("Sex")
                            Spacer()
                            Text("Male")
                        }
                        Divider()
                            .frame(height: 1)
                            .overlay(Color(UIColor.systemGray3))
                    }
                    .padding(.vertical, 10)
                    
                    VStack {
                        HStack {
                            Text("Height")
                            Spacer()
                            Text("160 cm")
                        }
                        Divider()
                            .frame(height: 1)
                            .overlay(Color(UIColor.systemGray3))
                    }
                    .padding(.vertical, 10)
                    
                    VStack {
                        HStack {
                            Text("Weight")
                            Spacer()
                            Text("60 kg")
                        }
                        Divider()
                            .frame(height: 1)
                            .overlay(Color(UIColor.systemGray3))
                    }
                    .padding(.vertical, 10)
                    
                    VStack {
                        HStack {
                            Text("Race Goal")
                            Spacer()
                            Text("5 KM Race")
                        }
                        Divider()
                            .frame(height: 1)
                            .overlay(Color(UIColor.systemGray3))
                    }
                    .padding(.vertical, 10)
                    
                    VStack {
                        HStack {
                            Text("Starting Level")
                            Spacer()
                            Text("Beginner")
                        }
                    }
                    .padding(.vertical, 10)
                }
                .font(.body)
                .padding()
                .overlay(RoundedRectangle(cornerRadius: 10)
                    .stroke(Color(UIColor.systemGray), lineWidth: 1))
                .padding(.horizontal, 25)
                
                Spacer()
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationTitle("Trainee Profile")
            .toolbar {
                Button("Dismiss") {
                    dismiss()
                }
            }
        }
    }
}

struct ProfileTraineeView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileTraineeView()
    }
}

//
//  ShareLinkViewModel.swift
//  coRun
//
//  Created by Irfan Izudin on 08/09/22.
//

import SwiftUI

class ShareLinkViewModel: ObservableObject {
    
    
    func shareLink(url: String){
        let activityVC = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        windowScene?.windows.first?.rootViewController?.present(activityVC, animated: true)
    }
}

//
//  ShareLinkView.swift
//  coRun
//
//  Created by Irfan Izudin on 07/09/22.
//

import SwiftUI

struct ShareLinkView: View {
    
    @StateObject var viewModelShareLink = ShareLinkViewModel()

    @AppStorage("uuid") var uuid = ""
    @State var link = ""
    
    var body: some View {
        
        VStack {
            Text("Share link to trainee, be a coach!")
                .font(.largeTitle)
                .multilineTextAlignment(.center)
                .frame(width: 294)
                .padding(.top, 40)
            
        
            VStack(alignment: .leading) {
                Text("Your link as a coach")
                    .font(.headline)
                
                HStack {
                    TextField("your link...", text: $link)
                        .padding()
                        .frame(height: 44)
                        .background(Color(UIColor.tertiarySystemFill))
                        .cornerRadius(10)
                        .textInputAutocapitalization(.never)
                        .disableAutocorrection(true)
                    
                    Button {
                        print("share link")
                        viewModelShareLink.shareLink(url: link)
                        print(link)

                        
                    } label: {
                        Image(systemName: "square.and.arrow.up")
                            .foregroundColor(.white)
                            .frame(width: 44, height: 44)
                            .background(Color("primaryGreen"))
                            .cornerRadius(10)

                    }

                }
            }
            .padding(.horizontal, 25)
            .padding(.top, 50)
            
            Spacer()
        }
        .onAppear() {
            link = "corunapp://coach=\(uuid)"
            print(uuid)
        }
            .navigationTitle("Share Link")
            .navigationBarTitleDisplayMode(.inline)
    }
}

struct ShareLinkView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ShareLinkView()
        }
    }
}

//
//  Trainee.swift
//  coRun
//
//  Created by Irfan Izudin on 12/09/22.
//

import Foundation

struct Trainee: Identifiable, Codable {
    let id: Int?
    let first_name: String?
    let last_name: String?
    let race_goal: Int?
    let status_assessment: String?
    let assessment_id: Int?
    let birth_date: String?
    let sex: Int?
    let height: Int?
    let weight: Int?
}

struct User: Identifiable, Codable {
    let id: Int?
    let first_name: String?
    let last_name: String?
    let birth_date: String?
    let sex: Int?
    let height: Int?
    let weight: Int?
}

struct DetailTrainee: Identifiable, Codable {
    var id: Int?
    var User: User
    let status_assessment: String?
    let race_goal: Int?
    var latest_running_date: String?
    var latest_running_indoor: Bool?
    var latest_running_distance: Float?
    var latest_running_pace: String?
    var latest_running_duration: String?
    var running_level: Int?
}

struct Assessment: Identifiable, Codable {
    var id: Int?
    var status: String?
}



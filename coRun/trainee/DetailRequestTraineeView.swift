//
//  DetailRequestTraineeView.swift
//  coRun
//
//  Created by Irfan Izudin on 12/09/22.
//

import SwiftUI

struct DetailRequestTraineeView: View {
    
    @StateObject var vmDetailRequestTrainee = DetailRequestTraineeViewModel()
    let assessment_id: Int
    @Environment(\.dismiss) var dismiss
    
    
    var body: some View {
        NavigationView {
            
            if vmDetailRequestTrainee.pendingTraineeIsLoading {
                ProgressView()
                    .navigationTitle("Training Request")
                    .navigationBarTitleDisplayMode(.inline)
                    .toolbar {
                        ToolbarItem {
                            Button {
                                dismiss()
                            } label: {
                                Image(systemName: "xmark")
                                    .foregroundColor(Color.primary)
                            }
                            
                        }
                    }

            } else {
                ScrollView {
                    HStack {
                        Image(systemName: "person.circle.fill")
                            .font(.system(size: 68))
                            .foregroundColor(Color("primaryGreen"))
                        Text((vmDetailRequestTrainee.detailTrainee?.User.first_name)! + " " + (vmDetailRequestTrainee.detailTrainee?.User.last_name)!)
                            .font(.title)
                            .fontWeight(.semibold)
                        Spacer()
                    }
                    .padding(.horizontal, 25)
                    
                    VStack {
                        HStack {
                            Text(vmDetailRequestTrainee.detailTrainee!.latest_running_date! + " | " + (vmDetailRequestTrainee.detailTrainee!.latest_running_indoor! ? "Indoor" : "Outdoor") + " Run")
                                .font(.subheadline)
                            Spacer()
                        }
                        .padding(.vertical, 5)
                        HStack {
                            VStack {
                                Text("\(vmDetailRequestTrainee.roundedDistance(distance: vmDetailRequestTrainee.detailTrainee!.latest_running_distance!)) km")
                                    .font(.title)
                                Text("Distance")
                                    .font(.caption)
                            }
                            Spacer()
                            VStack {
                                Text("\(vmDetailRequestTrainee.convertPace(pace: vmDetailRequestTrainee.detailTrainee!.latest_running_pace!))")
                                    .font(.title)
                                Text("Avg. Pace")
                                    .font(.caption)
                            }
                            Spacer()
                            VStack {
                                Text("\(vmDetailRequestTrainee.convertDuration(duration: vmDetailRequestTrainee.detailTrainee!.latest_running_duration!))")
                                    .font(.title)
                                Text("Duration")
                                    .font(.caption)
                            }
                        }
                    }
                    .padding()
                    .padding(.vertical, 10)
                    .overlay(RoundedRectangle(cornerRadius: 10)
                        .stroke(Color(UIColor.systemGray), lineWidth: 1))
                    .padding(.horizontal, 25)
                    .padding(.bottom)
                    
                    VStack {
                        VStack {
                            HStack {
                                Text("Age")
                                Spacer()
                                Text("\(vmDetailRequestTrainee.calculateAge(birth_date: vmDetailRequestTrainee.detailTrainee!.User.birth_date!))")
                            }
                            Divider()
                                .frame(height: 1)
                                .overlay(Color(UIColor.systemGray3))
                        }
                        .padding(.vertical, 10)
                        
                        VStack {
                            HStack {
                                Text("Sex")
                                Spacer()
                                Text(vmDetailRequestTrainee.detailTrainee?.User.sex == 0 ? "Male" : "Female")
                            }
                            Divider()
                                .frame(height: 1)
                                .overlay(Color(UIColor.systemGray3))
                        }
                        .padding(.vertical, 10)
                        
                        VStack {
                            HStack {
                                Text("Height")
                                Spacer()
                                Text("\(vmDetailRequestTrainee.detailTrainee!.User.height!) cm")
                            }
                            Divider()
                                .frame(height: 1)
                                .overlay(Color(UIColor.systemGray3))
                        }
                        .padding(.vertical, 10)
                        
                        VStack {
                            HStack {
                                Text("Weight")
                                Spacer()
                                Text("\(vmDetailRequestTrainee.detailTrainee!.User.weight!) kg")
                            }
                            Divider()
                                .frame(height: 1)
                                .overlay(Color(UIColor.systemGray3))
                        }
                        .padding(.vertical, 10)
                        
                        VStack {
                            HStack {
                                Text("Race Goal")
                                Spacer()
                                Text("\(vmDetailRequestTrainee.convertRaceGoal(race_goal: vmDetailRequestTrainee.detailTrainee!.race_goal!)) Race")
                            }
                            Divider()
                                .frame(height: 1)
                                .overlay(Color(UIColor.systemGray3))
                        }
                        .padding(.vertical, 10)
                        
                        VStack {
                            HStack {
                                Text("Starting Level")
                                Spacer()
                                Text("\(vmDetailRequestTrainee.convertRunningLevel(running_level: vmDetailRequestTrainee.detailTrainee!.running_level!))")
                            }
                            Divider()
                                .frame(height: 1)
                                .overlay(Color(UIColor.systemGray3))
                        }
                        .padding(.vertical, 10)
                        
                        
                    }
                    .font(.body)
                    .padding()
                    .padding(.vertical, 10)
                    .overlay(RoundedRectangle(cornerRadius: 10)
                        .stroke(Color(UIColor.systemGray), lineWidth: 1))
                    .padding(.horizontal, 25)
                    
                    Button {
//                        vmDetailRequestTrainee.updateStatusAssesment(id: assessment_id)
                        dismiss()
                    } label: {
                        Text("Accept")
                            .font(.headline)
                            .foregroundColor(.white)
                            .frame(height: 52)
                            .frame(maxWidth: .infinity)
                            .background(Color("primaryGreen"))
                            .cornerRadius(10)
                    }
                    .padding(.horizontal, 25)
                    .padding(.vertical, 10)
                    
//                    Button {
//                        print("decline")
//                    } label: {
//                        Text("Decline")
//                            .font(.headline)
//                            .foregroundColor(.white)
//                            .frame(height: 52)
//                            .frame(maxWidth: .infinity)
//                            .background(.red)
//                            .cornerRadius(10)
//                    }
//                    .padding(.horizontal, 25)
                    
                    
                    
                }
                .navigationTitle("Training Request")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem {
                        Button {
                            dismiss()
                        } label: {
                            Image(systemName: "xmark")
                                .foregroundColor(Color.primary)
                        }
                        
                    }
                }
                
            }
            
            
        }
        .onAppear(){
            vmDetailRequestTrainee.getDetailTrainee(id: assessment_id)
        }
        
    }
}

struct DetailRequestTraineeView_Previews: PreviewProvider {
    static var previews: some View {
        DetailRequestTraineeView(assessment_id: 25)
    }
}

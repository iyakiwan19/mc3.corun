//
//  ListTraineeView.swift
//  coRun
//
//  Created by Irfan Izudin on 08/09/22.
//

import SwiftUI

struct ListTraineeView: View {
    
    @StateObject var viewModelShareLink = ShareLinkViewModel()
    @StateObject var vmListTrainee = ListTraineeViewModel()
    @State private var showDetailRequestTrainee = false
    
    @State var selected_race: RaceType = .all
        
    @AppStorage("uuid") var uuid = ""
    @State var link = ""
    
    var body: some View {
        ScrollView {
            
            VStack {
                HStack {
                    Text("Training Request")
                        .font(.title3)
                        .fontWeight(.semibold)
                    Spacer()
                }
                                
                if vmListTrainee.pendingTraineeIsLoading {
                    ProgressView()
                    
                } else {
                    ForEach(vmListTrainee.pendingTrainee) { trainee in
                        TraineeCardView(trainee: trainee)
                            .onTapGesture {
                                print("id assesment =  ",trainee.assessment_id as Any)
                                vmListTrainee.assessmentId = trainee.assessment_id!
                                showDetailRequestTrainee.toggle()
                            }
                            .sheet(isPresented: $showDetailRequestTrainee) {
                                DetailRequestTraineeView(assessment_id: vmListTrainee.assessmentId)
                            }       
                    }
                }
                
            }
            .padding(20)
            
            VStack {
                HStack {
                    Text("Active Trainees")
                        .font(.title3)
                        .fontWeight(.semibold)
                    Spacer()
                }
                
                Picker("race goal", selection: $selected_race) {
                    ForEach(RaceType.allCases, id: \.self) {
                        Text($0.rawValue)
                    }
                }
                .pickerStyle(.segmented)
                
                if vmListTrainee.activeTraineeIsLoading {
                    ProgressView()
                    
                } else {
                    switch selected_race{
                    case .all:
                        ListTraineeFilterView(filterTrainee: vmListTrainee.approvedTrainee)
                        
                    case .fiveK:
                        ListTraineeFilterView(filterTrainee: vmListTrainee.fiveKTrainee)
                        
                    case .tenK:
                        ListTraineeFilterView(filterTrainee: vmListTrainee.tenKTrainee)
                        
                    case .halfMarathon:
                        ListTraineeFilterView(filterTrainee: vmListTrainee.halfMarathonTrainee)

                    case .fullMarathon:
                        ListTraineeFilterView(filterTrainee: vmListTrainee.fullMarathonTrainee)
                        
                    }
                }
                
                
                
                
            }
            .padding(20)
            
            
        }
        .navigationTitle("Trainee")
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            ToolbarItem {
                Button("Share Link") {
                    link = "corunapp://coach=\(uuid)"
                    print(uuid)
                    viewModelShareLink.shareLink(url: link)
                }
            }
        }
        
        
    }
}

struct ListTraineeView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ListTraineeView()
        }
    }
}

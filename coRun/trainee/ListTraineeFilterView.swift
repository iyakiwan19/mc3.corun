//
//  ListTraineeFilterView.swift
//  coRun
//
//  Created by Irfan Izudin on 13/09/22.
//

import SwiftUI

struct ListTraineeFilterView: View {
    
    var filterTrainee: [Trainee] = []
    
    var body: some View {
        ForEach(filterTrainee) { trainee in
            NavigationLink {
                DashboardTraineeView()
            } label: {
                TraineeCardView(trainee: trainee)
                    .foregroundColor(Color("textColor"))
            }
        }
    }
}

struct ListTraineeFilterView_Previews: PreviewProvider {
    
    static var listTrainee = [
        Trainee(id: 1, first_name: "Irfan", last_name: "Izudin", race_goal: 0, status_assessment: StatusAssessment.pending.rawValue, assessment_id: 1, birth_date: "1999-08-18", sex: 0, height: 180, weight: 75)
    ]
    
    static var previews: some View {
        ListTraineeFilterView(filterTrainee: listTrainee)
    }
}

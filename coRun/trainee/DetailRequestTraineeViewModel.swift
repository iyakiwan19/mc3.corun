//
//  DetailRequestTraineeViewModel.swift
//  coRun
//
//  Created by Irfan Izudin on 14/09/22.
//

import SwiftUI

class DetailRequestTraineeViewModel: ObservableObject {
    
    
    @Published var detailTrainee: DetailTrainee?
    
    @Published var pendingTraineeIsLoading = true
    
//    @AppStorage("coruntoken") var coruntoken = ""
    let coruntoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXVpZCI6IjAwMTk5MS42M2EzNGMzOGU1MjA0OGY2YWUxMzA2OTA5MDcwOWZjMi4wNDA0IiwiaWF0IjoxNjYzMjMzNDA3fQ.ZNpufudvzlOMyzARuiq6tN0MmLI7VNdWgS_DQPp3OGA"
        
    func getDetailTrainee(id: Int) {
        
//        let user = DetailTrainee(id: 1, User: User(id: 1, first_name: "Muftie", last_name: "Alie", birth_date: "1999-08-18", sex: 0, height: 189, weight: 79), status_assessment: StatusAssessment.pending.rawValue, race_goal: 2, latest_running_date: "2022-09-15", latest_running_indoor: true, latest_running_distance: 0.4775872230529785, latest_running_pace: "00:17:55", latest_running_duration: "00:03:09", running_level: 3)
//
//        self.detailTrainee = user
        pendingTraineeIsLoading = true


        let urlString = "https://mcedevs.com/assessment/assessment-trainee/\(id)"
        
        guard let url = URL(string: urlString) else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        request.setValue(coruntoken, forHTTPHeaderField: "coruntoken")

        URLSession.shared.dataTask(with: request) { data, res, error in
            do {
                if let data = data {
                    let result = try JSONDecoder().decode(DetailTrainee.self, from: data)
                    DispatchQueue.main.async {
                        print(result)
                        self.detailTrainee = result
                        self.pendingTraineeIsLoading = false
//                        completion("success")
                    }
                }

            } catch (let error) {
//                completion("Error")
                print(error.localizedDescription)
            }
        }
        .resume()

    }
    
    func convertRaceGoal(race_goal: Int) -> String {
        switch race_goal {
        case 0: return "5K"
        case 1: return "10K"
        case 2: return "Half Marathon"
        case 3: return "Marathon"
        default: return ""
        }
    }
    
    func convertRunningLevel(running_level: Int) -> String {
        switch running_level {
        case 0: return "Beginner"
        case 1: return "Intermediate"
        case 2: return "Advanced"
        case 3: return "Elite"
        default: return ""
        }
    }
    
    func calculateAge(birth_date: String) -> Int {
        let split = birth_date.split(separator: "-")
        let birth_year = Int(split[0])
        let current_year = Int(Date().formatted(with: "yyyy"))
        return current_year! - birth_year!
        
    }
    
    func roundedDistance(distance: Float) -> String {
        return String(round(distance * 10) / 10.0)
    }
    
    func convertPace(pace: String) -> String {
        let split = pace.split(separator: ":")
        let minutes = split[1]
        let seconds = split[2]
        return "\(minutes)'\(seconds)''"
    }
    
    func convertDuration(duration: String) -> String {
        let split = duration.split(separator: ":")
        let hour = split[0]
        let minutes = split[1]
        return "\(hour.last!)h \(minutes)m"
    }
    
    func updateStatusAssesment(id: Int) {
        let urlString = "https://mcedevs.com/assessment/assessment-trainee/\(id)"
        
        guard let url = URL(string: urlString) else {return}
        
        let body: [String: String] = ["status": "approved"]
        
        let finalData = try! JSONSerialization.data(withJSONObject: body)

        var request = URLRequest(url: url)
        request.httpMethod = "PATCH"
        request.httpBody = finalData
        
        request.setValue(coruntoken, forHTTPHeaderField: "coruntoken")

//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        URLSession.shared.dataTask(with: request) { data, res, error in
            do {
                if let data = data {
                    let result = try JSONDecoder().decode(Assessment.self, from: data)
                    print(result)
//                    DispatchQueue.main.async { [self] in
//                        coruntoken = result.accessToken
//                        print("coruntoken = ", coruntoken)
//
//                    }
                }

            } catch (let error) {
                print(error.localizedDescription)
            }
        }
        .resume()

    }
    
}

//
//  ListTraineeViewModel.swift
//  coRun
//
//  Created by Irfan Izudin on 13/09/22.
//

import SwiftUI

enum StatusAssessment: String {
    case approved = "approved"
    case pending = "pending"
    case rejected = "rejected"
}

enum RaceType: String, CaseIterable {
    case all = "All"
    case fiveK = "5K"
    case tenK = "10K"
    case halfMarathon = "21K"
    case fullMarathon = "42K"
}

class ListTraineeViewModel: ObservableObject {
    @Published var listTrainee: [Trainee] = []
    @Published var pendingTrainee: [Trainee] = []
    @Published var approvedTrainee: [Trainee] = []
    @Published var fiveKTrainee: [Trainee] = []
    @Published var tenKTrainee: [Trainee] = []
    @Published var halfMarathonTrainee: [Trainee] = []
    @Published var fullMarathonTrainee: [Trainee] = []
    @Published var pendingTraineeIsLoading = false
    @Published var activeTraineeIsLoading = false

    @Published var assessmentId: Int = 0
    
    init(){
        getTrainee(){ status in
            if status == "success"{
                self.filteredPendingTrainee()
                self.filteredApprovedTrainee()
                self.filteredFiveK()
                self.filteredTenK()
                self.filteredHalfMarathon()
                self.filteredFullMarathon()
                
            }
        }
        
        
    }
    
//    @AppStorage("coruntoken") var coruntoken = ""
    let coruntoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXVpZCI6IjAwMTk5MS42M2EzNGMzOGU1MjA0OGY2YWUxMzA2OTA5MDcwOWZjMi4wNDA0IiwiaWF0IjoxNjYzMjMzNDA3fQ.ZNpufudvzlOMyzARuiq6tN0MmLI7VNdWgS_DQPp3OGA"
    
    func getTrainee( completion: @escaping (String) ->() ) {
//        let user1 = Trainee(first_name: "Irfan", last_name: "Izudin", race_goal: 0, status_assessment: StatusAssessment.pending.rawValue)
//        let user2 = Trainee(first_name: "Mufti", last_name: "Alie", race_goal: 2, status_assessment: StatusAssessment.pending.rawValue)
//        let user3 = Trainee(first_name: "Ricky", last_name: "Suprayudi", race_goal: 0, status_assessment: StatusAssessment.approved.rawValue)
//        let user4 = Trainee(first_name: "Taufiq", last_name: "Ichwanusofa", race_goal: 1, status_assessment: StatusAssessment.approved.rawValue)
//        let user5 = Trainee(first_name: "Jono", last_name: "Ribowo", race_goal: 2, status_assessment: StatusAssessment.approved.rawValue)
//        let user6 = Trainee(first_name: "Budi", last_name: "Ronaldo", race_goal: 3, status_assessment: StatusAssessment.approved.rawValue)
//        let user7 = Trainee(first_name: "Guskoro", last_name: "Pradipta", race_goal: 0, status_assessment: StatusAssessment.approved.rawValue)
//        let user8 = Trainee(first_name: "Mufti", last_name: "Ichwanusofa", race_goal: 1, status_assessment: StatusAssessment.approved.rawValue)
//        let user9 = Trainee(first_name: "Messi", last_name: "Ribowo", race_goal: 2, status_assessment: StatusAssessment.approved.rawValue)
//        let user10 = Trainee(first_name: "Joko", last_name: "Ronaldo", race_goal: 3, status_assessment: StatusAssessment.approved.rawValue)
//        self.listTrainee.append(contentsOf: [user1,user2,user3,user4,user5,user6,user7,user8,user9,user10])
        pendingTraineeIsLoading = true
        activeTraineeIsLoading = true
        
        let urlString = "https://mcedevs.com/user/get-my-trainee"
        
        guard let url = URL(string: urlString) else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        request.setValue(coruntoken, forHTTPHeaderField: "coruntoken")

        URLSession.shared.dataTask(with: request) { data, res, error in
            do {
                if let data = data {
                    let result = try JSONDecoder().decode([Trainee].self, from: data)
                    DispatchQueue.main.async {
                        self.listTrainee = result
                        self.pendingTraineeIsLoading = false
                        self.activeTraineeIsLoading = false
                        completion("success")
                    }
                }

            } catch (let error) {
                completion("Error")
                print(error.localizedDescription)
            }
        }
        .resume()

    }
    
    func filteredPendingTrainee() {
        pendingTrainee = listTrainee.filter({ (trainee) -> Bool in
            return trainee.status_assessment == StatusAssessment.pending.rawValue
        })
    }
    
    func filteredApprovedTrainee() {
        approvedTrainee = listTrainee.filter({ (trainee) -> Bool in
            return trainee.status_assessment == StatusAssessment.approved.rawValue
        })
    }
    
    func filteredFiveK() {
        fiveKTrainee = approvedTrainee.filter({ (trainee) -> Bool in
            return trainee.race_goal == 0
        })
    }
    
    func filteredTenK() {
        tenKTrainee = approvedTrainee.filter({ (trainee) -> Bool in
            return trainee.race_goal == 1
        })
    }
    
    func filteredHalfMarathon() {
        halfMarathonTrainee = approvedTrainee.filter({ (trainee) -> Bool in
            return trainee.race_goal == 2
        })
    }

    func filteredFullMarathon() {
        fullMarathonTrainee = approvedTrainee.filter({ (trainee) -> Bool in
            return trainee.race_goal == 3
        })
    }


}

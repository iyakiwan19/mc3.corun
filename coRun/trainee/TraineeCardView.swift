//
//  TraineeCardView.swift
//  coRun
//
//  Created by Irfan Izudin on 12/09/22.
//

import SwiftUI

struct TraineeCardView: View {
    
    var trainee: Trainee
    @StateObject var vmListTrainee = ListTraineeViewModel()


    var body: some View {
        HStack {
            Image(systemName: "person.circle.fill")
                .font(.system(size: 25))
            Text(trainee.first_name! + " " + trainee.last_name!)
            Spacer()
            Image(systemName: "chevron.right")
        }
        .padding()
        .background(Color(UIColor.systemGray6))
        .cornerRadius(10)
        
    }
}

//struct TraineeCardView_Previews: PreviewProvider {
//    
//    static var previews: some View {
//        TraineeCardView(trainee: Trainee(id: 1, first_name: "Irfan", last_name: "Izudin", race_goal: 0, status_assessment: StatusAssessment.pending.rawValue))
//    }
//}

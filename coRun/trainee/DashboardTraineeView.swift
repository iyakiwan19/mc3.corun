//
//  DashboardTraineeView.swift
//  coRun
//
//  Created by Irfan Izudin on 12/09/22.
//

import SwiftUI

struct DashboardTraineeView: View {
    
    var body: some View {
        Text("Dashboard Trainee")
    }
}

struct DashboardTraineeView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardTraineeView()
    }
}

//
//  LoginView.swift
//  coRun
//
//  Created by Irfan Izudin on 29/08/22.
//

import SwiftUI
import AuthenticationServices

struct LoginView: View {
    
    @ObservedObject var viewModelLogin = LoginViewModel()
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.colorScheme) var colorScheme
    @State var showConfirmLogin = false
    @AppStorage("uuid") var uuid = ""
    @AppStorage("first_name") var first_name = ""
    @AppStorage("last_name") var last_name = ""

    var body: some View {
        NavigationView{
            VStack{
                Spacer()
                
                Image("logo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 200, alignment: .center)
                    .padding(.bottom, 37)
                
                HStack(spacing: 2) {
                    Text("co")
                        .font(.largeTitle)
                        .fontWeight(.semibold)
                        .foregroundColor(Color("primaryGreen"))
                    Text("run")
                        .font(.largeTitle)
                        .fontWeight(.semibold)
                        .foregroundColor(Color.gray)
                }
                Spacer()
                
//                SignInButtonView()
                SignInWithAppleButton(.signIn) { request in
                    
                    request.requestedScopes = [.email, .fullName]
                    
                } onCompletion: { result in
                    
                    switch result {
                    case .success(let auth):
                        switch auth.credential {
                        case let credential as ASAuthorizationAppleIDCredential:
                            let userId = credential.user
                            let email = credential.email
                            let firstName = credential.fullName?.givenName
                            let lastName = credential.fullName?.familyName
                            
                            uuid = userId
                            first_name = firstName ?? ""
                            last_name = lastName ?? ""
                            
                            print("userId = ", userId)
                            print("email = ", email ?? "")
                            print("name = \(firstName ?? "") \(lastName ?? "")")
                            print("uuid = \(uuid)")
                            if !uuid.isEmpty && (email ?? "").isEmpty && first_name.isEmpty && last_name.isEmpty {
                                print("uuid = \(uuid), email isEmpty, name isEmpty")
                                viewModelLogin.uuid = uuid
                                viewModelLogin.first_name = "_"
                                viewModelLogin.last_name = "_"
                                viewModelLogin.height = "0"
                                viewModelLogin.weight = "0"
                                viewModelLogin.selected_sex = "Male"
                                viewModelLogin.postData()
                                presentationMode.wrappedValue.dismiss()
                            } else {
                                showConfirmLogin = true
                            }
                        default:
                            break
                        }
                    case .failure(let error):
                        print(error)
                    }
                    
                }
                .signInWithAppleButtonStyle(colorScheme == .dark ? .white : .black)
                .frame(height: 52)
                .padding(25)
                .cornerRadius(10)
                .fullScreenCover(isPresented: $showConfirmLogin, onDismiss: {
                    presentationMode.wrappedValue.dismiss()
                    print("DetailFormView onDismiss")
                }) {
                    DetailFormView()
                }
                
                Spacer()
                
                Text("By signing up, you agree to coRun's Terms of Us and Privacy Policy")
                    .font(.caption)
                    .multilineTextAlignment(.center)
                    .foregroundColor(Color.gray)
                    .frame(width: 240)


            }
            .navigationTitle("Login")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem {
                    Button {
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        Image(systemName: "xmark")
                            .foregroundColor(colorScheme == .dark ? Color.white : Color.black)
                    }

                }
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

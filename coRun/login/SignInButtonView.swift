//
//  SignInButtonView.swift
//  coRun
//
//  Created by Irfan Izudin on 30/08/22.
//

import SwiftUI
import AuthenticationServices

struct SignInButtonView: View {

    @Environment(\.dismiss) var dismiss
    @Environment(\.colorScheme) var colorScheme
    @State var showConfirm = false
    @AppStorage("uuid") var uuid = ""
    @AppStorage("first_name") var first_name = ""
    @AppStorage("last_name") var last_name = ""
    
    var body: some View {
        SignInWithAppleButton(.signIn) { request in
            
            request.requestedScopes = [.email, .fullName]
            
        } onCompletion: { result in
            
            switch result {
            case .success(let auth):
                switch auth.credential {
                case let credential as ASAuthorizationAppleIDCredential:
                    let userId = credential.user
                    let email = credential.email
                    let firstName = credential.fullName?.givenName
                    let lastName = credential.fullName?.familyName
                    
                    uuid = userId
                    first_name = firstName ?? ""
                    last_name = lastName ?? ""
                    
                    print("userId = ", userId)
                    print("email = ", email ?? "")
                    print("name = \(firstName ?? "") \(lastName ?? "")")
                    showConfirm = true
                default:
                    break
                }
            case .failure(let error):
                print(error)
            }
            
        }
        .signInWithAppleButtonStyle(colorScheme == .dark ? .white : .black)
        .frame(height: 52)
        .padding(25)
        .cornerRadius(10)
        .fullScreenCover(isPresented: $showConfirm) {
            DetailFormView()
        }
    }
}

struct SignInButtonView_Previews: PreviewProvider {
    static var previews: some View {
        SignInButtonView()
    }
}

//
//  LoginViewModel.swift
//  coRun
//
//  Created by Irfan Izudin on 01/09/22.
//

import SwiftUI

class LoginViewModel: ObservableObject {
    @Published var user = [Login]()
    @Published var uuid = UserDefaults.standard.string(forKey: "uuid")
    @Published var first_name = UserDefaults.standard.string(forKey: "first_name") ?? ""
    @Published var last_name = UserDefaults.standard.string(forKey: "last_name") ?? ""
    @Published var selected_sex = "Male"
    @Published var birth_date = Date()
    @Published var dateFormatted = Date().formatted(with: "dd MMMM yyyy")
    @Published var height = ""
    @Published var weight = ""
    @Published var isShowPickerDate = false
    
    @AppStorage("coruntoken") var coruntoken = ""
    
    func convertSex(sex: String) -> Int {
        if sex == "Male" {
            return 0
        } else {
            return 1
        }
    }
    
    func convertDate(birthdate: Date) -> String {
        return birthdate.formatted(with: "yyyy-MM-dd")
    }
    
    func postData() {
        print("LoginViewModel::postData() uuid = \(uuid ?? "empty")")
        
        guard let uuid = uuid else { return }
        
        print(uuid, first_name, last_name, convertSex(sex: selected_sex), convertDate(birthdate: birth_date), height, weight)
        
        let urlString = "https://mcedevs.com/user/sign-in-apple"
        
        guard let url = URL(string: urlString) else {return}
        
        let body: [String: Any] = ["uuid": uuid, "first_name": first_name, "last_name": last_name, "sex": convertSex(sex: selected_sex), "birth_date": convertDate(birthdate: birth_date), "height": height, "weight": weight]
        
        let finalData = try! JSONSerialization.data(withJSONObject: body)

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = finalData
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        URLSession.shared.dataTask(with: request) { [self] data, res, error in
            do {
                if let data = data {
                    let result = try JSONDecoder().decode(Login.self, from: data)
                    print(result)
                    DispatchQueue.main.async { [self] in
                        coruntoken = result.accessToken
                        print("coruntoken = ", coruntoken)

                    }
                }

            } catch (let error) {
                print(error.localizedDescription)
            }
        }
        .resume()
    }


}

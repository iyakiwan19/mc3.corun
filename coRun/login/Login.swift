//
//  Login.swift
//  coRun
//
//  Created by Irfan Izudin on 05/09/22.
//

import Foundation

struct Login: Codable, Identifiable {
    let id: Int
    let uuid: String
    let first_name: String
    let last_name: String
    let sex: Int
    let birth_date: String
    let height: Int
    let weight: Int
    let accessToken: String
}

//
//  DetailFormView.swift
//  coRun
//
//  Created by Irfan Izudin on 30/08/22.
//

import SwiftUI


struct DetailFormView: View {
    
    @StateObject var viewModelLogin = LoginViewModel()
    
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.colorScheme) var colorScheme
    @Environment(\.dismiss) var dismiss
    @FocusState var keyboardFocus: Bool
    @AppStorage("coachID") var coachID = ""
    @State var showInputLinkView = false
    
    var disableForm: Bool {
        viewModelLogin.first_name.isEmpty || viewModelLogin.last_name.isEmpty || viewModelLogin.height.isEmpty || viewModelLogin.weight.isEmpty
    }
    
    let sex = ["Male", "Female"]
    
    
    var body: some View {
        NavigationView{
            
            ScrollView (.vertical, showsIndicators: false) {
                VStack (spacing: 20) {
                    VStack(alignment: .leading) {
                        Text("First name")
                            .font(.body)
                        TextField("Type here..", text: $viewModelLogin.first_name)
                            .padding()
                            .font(.body)
                            .frame(height: 44)
                            .background(Color(UIColor.tertiarySystemFill))
                            .cornerRadius(10)
                            .focused($keyboardFocus)
                            .disableAutocorrection(true)
                    }
                    
                    VStack(alignment: .leading) {
                        Text("Last name")
                            .font(.body)
                        TextField("Type here..", text: $viewModelLogin.last_name)
                            .padding()
                            .font(.body)
                            .frame(height: 44)
                            .background(Color(UIColor.tertiarySystemFill))
                            .cornerRadius(10)
                            .focused($keyboardFocus)
                            .disableAutocorrection(true)
                        
                    }
                    
                    VStack(alignment: .leading) {
                        Text("Sex")
                            .font(.body)
                        Picker("segmented", selection: $viewModelLogin.selected_sex) {
                            ForEach(sex, id: \.self) {
                                Text($0)
                            }
                        }
                        .pickerStyle(.segmented)
                    }
                    
                    VStack(alignment: .leading) {
                        HStack {
                            Text("Birthdate")
                                .font(.body)
                        }
                        TextField("Type here..", text: $viewModelLogin.dateFormatted)
                            .padding()
                            .font(.body)
                            .frame(height: 44)
                            .background(Color(UIColor.tertiarySystemFill))
                            .cornerRadius(10)
                            .focused($keyboardFocus)
                            .disabled(true)
                            .disableAutocorrection(true)
                            .onTapGesture {
                                withAnimation (.easeIn) {
                                    viewModelLogin.isShowPickerDate.toggle()
                                    viewModelLogin.dateFormatted = viewModelLogin.birth_date.formatted(with: "dd MMMM yyyy")

                                }
                            }
                        
                        if viewModelLogin.isShowPickerDate {
                            HStack {
                                Spacer()
                                Button {
                                    withAnimation (.easeIn) {
                                        viewModelLogin.isShowPickerDate.toggle()
                                        viewModelLogin.dateFormatted = viewModelLogin.birth_date.formatted(with: "dd MMMM yyyy")
                                    }
                                    
                                } label: {
                                    Text("Done")
                                }
                                
                            }
                            HStack{
                                Spacer()
                                DatePicker("", selection: $viewModelLogin.birth_date,in: ...Date(), displayedComponents: .date)
                                    .datePickerStyle(.wheel)
                                    .labelsHidden()
                                Spacer()
                            }
                        } else {
                            
                        }
                        
                        
                    }
                    
                    VStack(alignment: .leading) {
                        Text("Height (cm)")
                            .font(.body)
                        TextField("Type here..", text: $viewModelLogin.height)
                            .padding()
                            .font(.body)
                            .frame(height: 44)
                            .background(Color(UIColor.tertiarySystemFill))
                            .cornerRadius(10)
                            .keyboardType(.numberPad)
                            .focused($keyboardFocus)
                            .disableAutocorrection(true)
                        
                    }
                    
                    VStack(alignment: .leading) {
                        Text("Weight (kg)")
                            .font(.body)
                        TextField("Type here..", text: $viewModelLogin.weight)
                            .padding()
                            .font(.body)
                            .frame(height: 44)
                            .background(Color(UIColor.tertiarySystemFill))
                            .cornerRadius(10)
                            .keyboardType(.numberPad)
                            .focused($keyboardFocus)
                            .disableAutocorrection(true)
                        
                    }
                    
                    Button {
                        viewModelLogin.postData()
                        dismiss()
                    } label: {
                        Text("Finish")
                            .font(.headline)
                            .foregroundColor(.white)
                            .frame(maxWidth: .infinity)
                            .frame(height: 52)
                            .background(Color("primaryGreen"))
                            .opacity(disableForm ? 0.5 : 1)
                            .cornerRadius(10)
                    }
                    .padding(.top, 30)
                    .disabled(disableForm)
                }
                .navigationTitle("Confirm Your Details")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
//                    ToolbarItem {
//                        Button {
//                            dismiss()
//                        } label: {
//                            Image(systemName: "xmark")
//                                .foregroundColor(Color.primary)
//                        }
//
//                    }
                    ToolbarItemGroup(placement: .keyboard) {
                        Spacer()
                        Button("Done") {
                            keyboardFocus = false
                        }
                    }
                }
                
            }
            .padding(25)
        }
    }
}

struct DetailFormView_Previews: PreviewProvider {
    static var previews: some View {
        DetailFormView()
    }
}

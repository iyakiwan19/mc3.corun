//
//  coRunApp.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 25/08/22.
//

import SwiftUI

//enum ActiveSheet: Identifiable {
//    case login, inputLink
//
//    var id: Int {
//        hashValue
//    }
//}

@main
struct coRunApp: App {
    
    @StateObject var deepLinkData: DeepLinkModel = DeepLinkModel()
    @State var showLogin = false
    @State var showInputLink = false
//    @State var activeSheet: ActiveSheet?
    @AppStorage("uuid") var uuid = ""
    @AppStorage("coachID") var coachID = ""

    var body: some Scene {
        WindowGroup {
            AppNavigationView()
                .onOpenURL { url in
                    print("incoming url: \(url)")
                    if deepLinkData.checkDeepLink(url: url) {
                        print("From Deep Link = \(deepLinkData.coachID)")
                        if uuid.isEmpty {
//                            activeSheet = .login
                            showLogin = true
                            showInputLink = true
                        } else {
                            print("inputLink from Deep Link")
//                            activeSheet = .inputLink
                            showLogin = false
                            showInputLink = true
                        }
                    } else {
                        print("Fall Back Deep Link")
                    }
                }
//                .fullScreenCover(item: $activeSheet, onDismiss: {
//                    if !uuid.isEmpty {
//                        if coachID.isEmpty {
//                            print("onOpenURL coachID = empty")
//                            activeSheet = .inputLink
//                        } else {
//                            print("onOpenURL coachID = \(coachID), lanjut ke assesment")
//                        }
//                    }
//                }) { item in
//                    switch item {
//                    case .login:
//                        LoginView()
//                    case .inputLink:
//                        InputLinkView(coachLink: deepLinkData.coachID, submitLinkButtonIsDisabled: false)
//                    }
//                }
                .fullScreenCover(isPresented: $showLogin, onDismiss: {
                    print("onOpenURL LoginView onDismiss, UUID = \(uuid)")
                        if !uuid.isEmpty {
                            showInputLink = true
                            print("onOpenURL showInputLink = \(showInputLink)")
                        } else {
                            showInputLink = false
                        }
                }) {
                    LoginView()
                }
                .fullScreenCover(isPresented: $showInputLink, onDismiss: {
                    if !coachID.isEmpty {
                        print("onOpenURL coachID = \(coachID), lanjut ke assesment")
                    } else {
                        print("onOpenURL coachID = empty")
                    }
                }) {
                    InputLinkView(coachLink: deepLinkData.coachID, submitLinkButtonIsDisabled: false)
                }
        }
    }
}

//Integrate Get Training Plan do LoginView if UUID not exist
//DetailFormView modals closed if UUID exists
//InputLinkView if coachID no exist, button Clear & Done on top of keyboard
//DetailFormView xmark button removed
//SignInButtonView directly put inside LoginView for modal dismiss continuity

//Integrate Get Training Plan do LoginView if UUID not exist
//DetailFormView modals closed if UUID exists
//InputLinkView if coachID no exist, button Clear & Done on top of keyboard
//DetailFormView xmark button removed
//SignInButtonView directly put inside LoginView for modal dismiss continuity

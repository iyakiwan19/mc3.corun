//
//  Date.swift
//  coRun
//
//  Created by Irfan Izudin on 05/09/22.
//

import Foundation

extension Date {
    func formatted(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

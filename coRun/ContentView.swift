//
//  ContentView.swift
//  coRun
//
//  Created by Mufti Alie Satriawan on 25/08/22.
//

import SwiftUI

struct ContentView: View {
    
    @State var tabbar: UITabBarController?
    
    var body: some View {
        NavigationLink {
//            ShareLinkView()
            ListTraineeView()
        } label: {
            Text("Trainee")
                .font(.headline)
                .foregroundColor(Color.white)
                .padding()
                .background(Color("primaryGreen"))
                .cornerRadius(10)
        }

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//
//  LineChartView.swift
//  coRun
//
//  Created by Taufiq Ichwanusofa on 13/09/22.
//

import SwiftUI

struct LineChartView: View {
    let data: [SummaryModel] = [
        SummaryModel(week: 0, distance: 19.5),
        SummaryModel(week: 1, distance: 23.0),
        SummaryModel(week: 2, distance: 23.3),
        SummaryModel(week: 3, distance: 24.15),
        SummaryModel(week: 4, distance: 20.0),
        SummaryModel(week: 5, distance: 25.0),
        SummaryModel(week: 6, distance: 32.0)
    ]
    
    let highestPoint: Double {
        return data.max { $0.distance < $1.distance }?.distance
    }
    
    private func ratio(for index: Int) -> Double {
        1 - (data[index] / highestPoint)
      }
    
    var body: some View {
        GeometryReader { geometry in
            let height = geometry.size.height
            let width = geometry.size.width
            
            Path { path in
                path.move(to: CGPoint(x: 0, y: height * self.ratio(for: 0)))
                
                for index in 1..<data.count {
                    path.addLine(to: CGPoint(
                        x: CGFloat(index) * width / CGFloat(data.count - 1),
                        y: height * self.ratio(for: index)
                    ))
                }
            }
            .stroke(Color.accentColor, style: StrokeStyle(lineWidth: 2, lineJoin: .round)
        }
        .padding(.vertical)
    }
}

struct LineChartView_Previews: PreviewProvider {
    static var previews: some View {
        LineChartView()
    }
}
